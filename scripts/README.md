# InspiralHOM

The measurement of sub-leading modes of gravitational wave signals from compact binary
coalescences observed in Advanced LIGO.

# Installation:

1.   **Requirements**: LALSuite, PyCBC, PyCWT, fitter, mpmath, ligo-gracedb
```sh
mkdir test_insphom
virtualenv test_insphom/
source test_insphom/bin/activate
pip install lalsuite
pip install pycbc
pip install pycwt
pip install fitter 
pip install mpmath
pip install ligo-gracedb 
pip install pycbc-glue python-cjson Cython M2Crypto 
```
2. **Optional:** LALSuite extra (necessary to use the ROM data)
```sh
cd ${VIRTUAL_ENV}/
mkdir src
cd ${VIRTUAL_ENV}/src
git-lfs clone https://git.ligo.org/lscsoft/lalsuite-extra.git
cd lalsuite-extra
./00boot
./configure --prefix=${VIRTUAL_ENV}/opt/lalsuite-extra
make install
echo 'export LAL_DATA_PATH=${VIRTUAL_ENV}/opt/lalsuite-extra/share/lalsimulation' >> ${VIRTUAL_ENV}/bin/activate
```



# Example 

* **GW190412**: intrinsic parameters are obtained from SEOBNRv4HM PE study.
```sh
python inspiralhom_analysis.py --detector L1 \
        --frame-type L1_HOFT_C00 --channel-name L1:GDS-CALIB_STRAIN \
        --window-width 34000 --number-threads 70 \
        --approximant SEOBNRv4HM \
        --track-end-frequency 0.6fISCO \
        --track-max-length 0.45 \
        --mass1 33.7064121235 --mass2 9.91577878517 \
        --spin1z 0.328604894804 --spin2z 0.00771914998989 \
        --event-time 1239082262.18 \
        --det-event-time 1239082262.164152 \
        --output-filename S190412m_L1_C00_0p6fISCO_0p45.h5
```
Or use the summarry statistics file obtained from PE study
```sh
## Link of the summary statistics page
## https://galahad.aei.mpg.de/~rcotesta/LVC/offline_pe/S190412m/SEOBNRv4HM_ROM_nest/SEOBNRv4HM_ROMpseudoFourPN/1239082262.22-329473/V1H1L1/summary_statistics.dat

python inspiralhom_analysis.py --detector L1 \
            --frame-type L1_HOFT_C00 --channel-name L1:GDS-CALIB_STRAIN \
            --window-width 34000 --number-threads 70 \
            --approximant SEOBNRv4HM \
            --track-end-frequency 0.6fISCO \
            --track-max-length 0.45 \
            --posterior-summary-file summary_statistics.dat \
            --posterior-param-type maxL  \
            --output-filename S190412m_L1_C00_0p6fISCO_0p45.h5
```

## Use condor submission

    1.  Save the above command in a bash script <test_insphom.sh>.
    2.  Describes the job in a '.sub' file <run_job.sub> that will be running .
```sh
executable = test_insphom.sh
universe = vanilla
request_memory = 300000M
getenv = True
accounting_group = ligo.dev.o3.cbc.explore.test
log = foo1.log
output = foo1.output
error = foo.err
priority = 1 
queue 

```
    3.  Submit the job
```sh
source test_insphom/bin/activate
chmod +x test_insphom_GW190412_C00_v2.sh
condor_submit run_job.sub
```
*It may take log time to start the job due to the request of large memory*