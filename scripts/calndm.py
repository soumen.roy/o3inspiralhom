"""
Calculate the nondimensional frequency of the Morlet wavelet for continious wavelet transformation (CWT)
for which the energy along the (2, 2) time-frequency track is maximim.

Reference for nondimensional frequency of the Morlet wavelet
for CWT: https://journals.ametsoc.org/doi/abs/10.117/1520-0477%281998%29079%3C0061%3AAPGTWA%3E2.0.CO%3B2, 
http://paos.colorado.edu/research/wavelets/bams_79_01_0061.pdf

Morlet wavelet: \Psi(t) = \pi^(-1/4)  \exp{ i f_0 t } \exp{-t^2 /2}, where f_0 is 
nondimensional frequency
"""

from __future__ import division
import pycwt
import numpy as np
from scipy import optimize

from pycbc.filter import resample_to_delta_t
import pycbc.psd 
import pycbc.frame

__author__ = "Soumen Roy <soumen.roy@ligo.org>"

import utils
mUtils = utils.MiscellaneousUtils()

def GenerateCWT(ndm_freq, ts, tindx, findx, cwt_fhigh, deltaF, deltaT):
    """
    This function produces the time-frequency map of a time-series using the 
    continious wavelet transformation (CWT) and then summing the pixels eneergies along
    a track. The CWT uses the Morlet wavelet.
    
    
    Parameters
    ----------
    ndm_freq: float
        Nondimensional frequency of the Morlet wavelet. Unit: dimensionless
    ts: numpy.array
        Array of the time-series. Unit: Sec
    tindx: numpy.array
        Time indices of the track. Unit: dimensionless
    findx: numpy.array
        Frequency indices of the track. Unit: dimensionless
    cwt_fhigh: float
        Higghest frequency for the computation of CWT. Unit: Hz
    deltaF: float
        Frequency sampling. Unit: Hz
    deltaT: float
        Time sampling. Unit: Sec
        
    Return
    ------
         Return the negative of the total power along the provided track indices.
        
    """
    
    wave =  mUtils._compute_cwt(ts, ndm_freq=ndm_freq, cwt_fhigh=cwt_fhigh, \
                                deltaT=deltaT, norm=False)[0]
    power = (np.abs(wave))**2

    # Subtract the frequency index by (1.0/deltaF) since the time-frequency map is constructed 
    # between 1Hz and cwt_fhigh
    negativeEng =  -np.sum(power[findx-int(1.0/deltaF), tindx])
    return negativeEng


def FindNonDimensionalFrequency(msUtils, ts, track_length=1.0, track_end_frequency=None):
    """
    Find the nondimensional frequency of the Morlet wavelet.
    
    parameters
    ----------
    ts: numpy.array
        Time series of the data.
        
    track_length: float
        Length of the time-frequency track of the (2, 2) mode. default 1.0 Sec.
        We assume the track is ending at fISCO.
        
    Return:
    -------
    ndf: float
        Nondimensional frequency of the Morlet wavelet
    

    """
    if track_end_frequency is None:
        track_end_frequency = msUtils.track_end_frequency
        
    tArrFixDT, fArrFixDT, intpF, intpT = msUtils._gen_time_frequency_path(start_freq=msUtils.fmin, \
                         track_max_length=track_length, track_end_frequency=track_end_frequency)
    
    
    # Get the time frequency indices for \alpha = 1
    alpha = 1.0
    time_index, freq_index =  msUtils._convert_tfpath_to_index(alpha, tArrFixDT, fArrFixDT, intpT)

    # Find the non-dimensional frequency of the Morlet wavelet for which the energy along the 22-track is maximum
    sol = optimize.minimize_scalar(GenerateCWT, bounds=[5, 50], method='bounded', options={'xatol': 1e-03}, \
                        args=(ts, time_index, freq_index, int(fArrFixDT[-1]*2), msUtils.deltaF, msUtils.deltaT))
    ndf = sol.x
    
    return ndf




