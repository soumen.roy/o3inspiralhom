"""
Noise alpha-series [N(alpha)] generator using the off-source data surrounding 
the event time. 
"""
from __future__ import division
from multiprocessing import Process, Manager
import pycbc
from pycbc.filter import resample_to_delta_t
from pycbc.events import veto
import numpy as np
import utils
import time

__author__ = "Soumen Roy <soumen.roy@ligo.org>"


class GenerateNoiseBG( utils.MiscellaneousUtils ):
    """
    A class object for gnerating the noise samples N(alpha) from the off-source data.
    
    Scheme:
     <-------- off-source ----------><--on-source --><--------- off-source --------->
    ||--:--:--:--:--||--:--:--:--:--||--:--:--:--:--||--:--:--:--:--||--:--:--:--:--||
    
    
    1. Extract the off-source data with a length of chunk_length. ||--:--:--:--:--||
    2. Compute the PSD using the Welch median method.
    3. Whiten the chunk.
    4. Remove 2s of data from both the beginning and end.
    5. Slices into 'n' sub-chunks with a length of sub_chunk_length. :--:
    6. Calculate N(alpha) from the time-frequency representation of the sub-chunk.

    """
    def __init__(self, misutils):
        for x, v in misutils.__dict__.copy().items():
            setattr(self, x, v) 
        
    
    def _check_science_segment_veto_condition(self, start, dur, science_start_end=None, veto_start_end=None):
        """
        This function checks whther the science data is available during a time period,
        and also checks veto.
        
        Parameters
        -----------
        start: float or int
            GPS start time of the segment. UNIT: Sec
        dur: float or int
            Time duration of the segment. UNIT: Sec
        science_start_end: list
            List of two numpy.array, first and second arrays contain
            the starting and ending GPS times of the science data, respectively.
            UNIT: Sec
        veto_start_end: list
            List of two numpy.array, first and second arrays contain
            the starting and ending GPS times of the veto, respectively.
            UNIT: Sec
        """
        end = start + dur
        
        if science_start_end is not None:
            # check the start-end time inside any science segment.
            idx = np.searchsorted(science_start_end[0], start, side='right') - 1
            if science_start_end[1][idx]>= start >= science_start_end[0][idx] and \
                            science_start_end[1][idx] >= end :
                science_condition = True
            else:
                science_condition = False
        else:
            science_condition = True
            
        if veto_start_end is not None:
            # check the start-end time inside any veto segment.
            idx = np.searchsorted(veto_start_end[0], start, side='right') - 1
            if veto_start_end[1][idx]>= start >= veto_start_end[0][idx] and \
                            veto_start_end[1][idx] >= end :
                veto_condition = False
            else:
                # Check any starting or ending time of veto is between the start-end time.
                idx1 = np.where(np.logical_and(veto_start_end[0]>=start, veto_start_end[0]<=end))[0]
                idx2 = np.where(np.logical_and(veto_start_end[1]>=start, veto_start_end[1]<=end))[0]

                if len(idx1) + len(idx2) == 0:
                    veto_condition = True
                else:
                    veto_condition = False
        else:
            veto_condition = True
        
        if science_condition is True and veto_condition is True:
            return True
        else:
            return False
            
                
    def _compute_nalpha_series_from_time_series(self, wStrain, tftracks, psd, tArrFixDT, fArrFixDT, interpT):
        """
        This function generates the N(\alpha) series from the whitened strain.
        The raw strain is whitened by using its PSD (welch median estimation) and then
        slices the whiten strain into 'int( (duration of raw strain - 4) * deltaF))'
        pieces. Compute the CWT of each small chunk of length '1/deltaF' and then summed
        the pixels energies along various scaled tracks.
        
        parameters
        -----------
        wStrain: pycbc.TimeSeries
            Whiten starin of the detector.
        tftracks: numpy.ndarray
            Indices of all scaled time-frequency tracks 
            
        Return 
        -------
        nalpha_series: numpy.ndarray
            Each row of nalpha_series is a single N(\alpha) series.
        """
        
        self.mode_list = [ (3, 3)]
        Salpha33 = self._gen_template_model(psd, tArrFixDT, fArrFixDT, interpT)[0]

        self.mode_list = [ (4, 4)]
        Salpha44 = self._gen_template_model( psd, tArrFixDT, fArrFixDT, interpT)[0]

        for jj in range( int(wStrain.duration*self.deltaF) ):
                
            sub_chunk = wStrain.data[ int(jj/self.deltaF/self.deltaT):int( (jj+1) /self.deltaF/self.deltaT)  ]
            # Continuous wavelet transform of the data
            wave = self._compute_cwt(sub_chunk, norm=True)[0]
            power = np.abs(wave)**2
                
            # Stack the engies along different scaled tracks
            stack_eng = np.array([ np.sum(power[tftracks[kk][1]- int(1.0/self.deltaF), tftracks[kk][0]]) \
                                        for kk in range( len(tftracks) ) ])
                                    
            try: nalpha_series = np.vstack(( nalpha_series, stack_eng))
            except: nalpha_series = stack_eng

        return [nalpha_series, Salpha33, Salpha44]
    
    
    def _parallel_helper(self, args, dic, key):
        """
        Internal helper function for parallel compuation of N(\alpha)
        """
        dic[key] = self._compute_nalpha_series_from_time_series(args[0], args[1], args[2], args[3], args[4], args[5])
    
    
    def _gen_whitened_strain_from_raw(self, strain, crop_length=2):
        
        # Calculate the noise power spectral density
        psd = pycbc.psd.interpolate(pycbc.psd.welch(strain), 1.0/strain.duration)
        
        # Whiten the chunck and remove 2 seconds of data from both the beginning and end
        wStrain = self._whiten(strain, psd)
        wStrain = wStrain.crop(crop_length, crop_length)
        
        return wStrain, psd
    
        
    def main(self, tArrFixDT, fArrFixDT, intpT, gps_start_time, gps_end_time, avoid_length=32, \
              nprocess=1, science_segment_file=None, veto_segment_file=None):
        """
        This function generates many N(\alpha) samples from the off-source data. 
        First, we slices the raw strain into many segments. Whiten each segment 
        using their own PSD. We avoid the data surrounding the event time 
        ( event_time-avoid_length, event_time+avoid_length ).
        
        
        Parameters:
        -----------
        tArrFixDT, fArrFixDT: numpy.array
            tArrFixDT, fArrFixDT are time and frequency samples respectively 
            of f(t) curve at a fixed time step.
        intpT: Class object
            scipy.interpolate.interp1d object of t(f) curve.
        gps_start_time: float or int
            The GPS start time of the time series. UNIT: Sec
        avoid_length: int
            Avoid the data either side of the event time (\pm avoid_length).
        nprocess: int
            Number of usabel processors.
        science_segment_file: str
            A string that contains the path to xml files that containa
            a segment table of science data.
        veto_segment_file: str
            A string that contains the path to xml file that contains
            a segment veto table.
            
        """

        # Generate the indices for each scaled track
        trackData = []
        for alpha in self.alpha_arr:
            time_index, freq_index = self._convert_tfpath_to_index(\
                    alpha, tArrFixDT, fArrFixDT, intpT)
            trackData.append( [time_index, freq_index] )
        
        # start and end time arrays from a science segment file.
        if science_segment_file is not None:
            science_start_end = veto.start_end_from_segments(science_segment_file)
        else:
            science_start_end = None
            
        # start and end time arrays from a veto segment file.
        if veto_segment_file is not None:
            veto_start_end = veto.start_end_from_segments(veto_segment_file)
        else:
            veto_start_end = None
            
        # Fetch the raw strain
        raw_strain = pycbc.frame.query_and_read_frame(self.frame_type, self.channel_name, \
                                 int(gps_start_time-2), int(gps_end_time+2))
        
        # Resample the data to required rate
        if raw_strain.delta_t != self.deltaT:
            raw_strain = resample_to_delta_t(raw_strain, self.deltaT)
        raw_strain = raw_strain.crop(2, 2)
            
        # Array of start_time and end_time of all the chunks
        # Avoid the on-source data
        segment_times1 = np.arange(int(self.det_event_time-avoid_length-self.chunk_length), \
                                  int(gps_start_time), -self.chunk_length) - gps_start_time
        segment_times2 = np.arange(int(self.det_event_time+avoid_length), \
                                  int(gps_end_time -self.chunk_length), self.chunk_length) - gps_start_time
        segment_times = np.sort( np.hstack(( segment_times1, segment_times2))).astype(int)
        
        # Initialize a dictonary to contain the N(alpha) samples for each start time
        # of the chunk from off-source data
        nalpha_samples = {}
        if nprocess == 1:  
            for seg_start in segment_times:
                wchunk, psd = self._gen_whitened_strain_from_raw( pycbc.types.TimeSeries( \
                                  raw_strain.data[int( seg_start/self.deltaT): \
                                       int((seg_start+self.chunk_length)/self.deltaT)], self.deltaT) )
                             
                seg_start_time = seg_start + gps_start_time
                chkVeto = self._check_science_segment_veto_condition(seg_start_time, \
                                         self.chunk_length, science_start_end, veto_start_end)
                             
                # Avoid the chunk which has a high value after whitening. 
                # Similar approach is used in PyCBC for matched filtering the data
                # This technique is called 'gating', see <https://arxiv.org/pdf/1508.02357.pdf#subsection.3.2>
                # The whitened LIGO data assumed to be Gaussian random variables N(0, 1).
                # The whitened time-series with a value greater than 4 is very less probable, which can
                # occur from the noise transient. 
                # We remove the time-series which has a value greater than 6.
                if chkVeto is True and np.max(np.abs(wchunk.data))<6:
                    nalpha_series = self._compute_nalpha_series_from_time_series(\
                                      wchunk, trackData, psd, tArrFixDT, fArrFixDT, intpT)
                    nalpha_samples[seg_start_time] = nalpha_series
                
        else:
            # Parallel scheme
            mgr = Manager()
            dic = mgr.dict()
            idx = 0
            for ii in range( int(len(segment_times)//nprocess \
                             + min(1, len(segment_times)%nprocess)) ):
                jobs = []; jj = 0
                while jj <= nprocess and idx < len(segment_times):
                    seg_start = segment_times[idx]
                    wchunk, psd = self._gen_whitened_strain_from_raw( pycbc.types.TimeSeries( \
                                  raw_strain.data[ int( seg_start/self.deltaT): \
                                       int((seg_start+self.chunk_length)/self.deltaT)], self.deltaT) )
                    
                    seg_start_time = seg_start + gps_start_time
                    chkVeto = self._check_science_segment_veto_condition(seg_start_time, \
                                         self.chunk_length, science_start_end, veto_start_end)
                                        
                    # Avoid the chunk which has a high value after whitening. 
                    # Similar approach is used in PyCBC for matched filtering the data
                    # This technique is called 'gating', see <https://arxiv.org/pdf/1508.02357.pdf#subsection.3.2>
                    # The whitened LIGO data assumed to be Gaussian random variables N(0, 1).
                    # The whitened time-series with a value greater than 4 is very less probable, which can
                    # occur from the noise transient. 
                    # We remove the time-series which has a value greater than 6.
                    if chkVeto is True and np.max(np.abs(wchunk.data))<6:
                        
                        jobs.append( Process( target=self._parallel_helper, \
                             args=( [wchunk, trackData, psd, tArrFixDT, fArrFixDT, intpT], dic, seg_start_time) ) )
                        jj += 1                        
                    idx += 1
                    
                for job in jobs: job.start()
                for job in jobs: job.join() 
                    
            
            for key in dic.keys():
                if len(dic[key]) != 0:
                    nalpha_samples[key] = dic[key]
        return nalpha_samples
        
        
