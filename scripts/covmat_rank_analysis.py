"""
This program analyses the rank of the covariance matrix, which uses the postprocessing
ouput data generated from the main analysis script inspiralhom_analysis.py. It produces
statistic values for a chosen value of rank. Finally, this program produces the results
for all possible rank of the covaraince matrix.
"""

from __future__ import division
import sys
import argparse
from ConfigParser import ConfigParser
import numpy as np
import h5py
import scipy.optimize as opt

import utils


__author__ = "Soumen Roy <soumen.roy@ligo.org>"
__program__ = "read_insphom_results"
__version__ = "no version"

usage = """ 

python covmat_rank_analysis.py --input-file gw190814_L1_C01_4096_0p6fISCO_0p45.h5 \
                        --output-file gw190814_rank_analysis.h5 \
                        --covmat-rank 150 \
                        
python covmat_rank_analysis.py --input-file gw190412_L1_C01_60Hz_4096_0p6fISCO_0p45.h5 \
                        --output-file gw190412_rank_analysis.h5 \
                        --covmat-rank 150
                        
"""

parser = argparse.ArgumentParser(description=__doc__[1:], \
    formatter_class=argparse.ArgumentDefaultsHelpFormatter, usage=usage)

parser.add_argument("--input-file",
                type=str, default=None, required=True, action="store",
                help="Supply the name of the input hdf5 file. REQUIRED")


parser.add_argument("--output-file",
                type=str, default=None, required=True, action="store",
                help="Supply the name of the output hdf5 file. REQUIRED")


parser.add_argument("--covmat-rank",
                type=int, default=None, action="store", 
                help=" Provide the reduced rank of the covariance matrix. "
                     " This parameter is required to compute the inverse "
                     " of the ill-conditioned covariance matrix by using "
                     " the Moore-Penrose pseudo-inverse method. OPTIONAL. ")


args = parser.parse_args()

# check the file format
if args.input_file.endswith('h5'):
    pass
elif args.input_file.endswith('hdf5'):
    pass
else:
    raise ValueError('Input file must be hdf5 format')
    
    
# Load the input file
f = h5py.File(args.input_file, 'r')

Salpha22 = f['analysis']['Salpha22'][:]
Salpha33 = f['analysis']['Salpha33'][:]
Salpha44 = f['analysis']['Salpha44'][:]
Yalpha = f['analysis']['Yalpha']

Nalpha_dataFulll = f['noisebg']['nalphas'][:][:,1:,]

Nalpha_dataFull = Nalpha_dataFulll.copy()
times = np.delete(f['noisebg']['nalphas'][:][:,0], [], axis=0) 


midIdx = np.where(times > f['inputparams']['t0'][()] )[0][0]
deltaIdx = 1200
Nalpha_data = Nalpha_dataFull.copy()[midIdx-deltaIdx:midIdx+deltaIdx]

# Mean at each \alpha
mualpha = np.mean(Nalpha_data, axis=0)
xy = np.vstack([Nalpha_data[:,j] - mualpha[j] for j in range(len(mualpha))])

# Covariance matrix
npcov = np.cov(xy)

# Compute the Frobenius norm of the residual matrix
u, s, vh = np.linalg.svd(npcov, full_matrices=True)
npcov2 = np.dot( u[:,:args.covmat_rank], np.dot( np.diag(s[:args.covmat_rank]), vh[:args.covmat_rank, :]))

cov_norm = np.linalg.norm( npcov, 'fro'  )
res_cov_norm = np.linalg.norm( npcov - npcov2, 'fro'  )
sys.stdout.write("Rank of the reduced covariance matrix = %s \n" % args.covmat_rank )
sys.stdout.write("Frobenius norm of the covariance matrix = %s \n" %cov_norm  )
sys.stdout.write("Frobenius norm of the residual matrix = %s \n\n" %res_cov_norm  )
 


# Compute inverse of covariance matrix using SVD based Moore-Penrose pseudo-inverse method
if args.covmat_rank is None:
    covinv = np.linalg.pinv(npcov)
else:
    covinv = np.linalg.pinv(npcov, rcond= s[args.covmat_rank]/max(s) )





# Maximize the LLR for signal model m=2 mode.
initial_guess = [1.0]
res22 = opt.minimize(utils.NegativeLogLikelihoodRatio, initial_guess, \
            args=[ [Salpha22], Yalpha, covinv, mualpha], method='Powell', \
            options={'ftol': 1e-20} )

# Maximize the LLR for m=2 and m=3 modes.
initial_guess = [1.0, 1.0]
res22_33 = opt.minimize(utils.NegativeLogLikelihoodRatio, initial_guess, \
                   args=[ [Salpha22, Salpha33], Yalpha, covinv, mualpha], \
                   method='Powell', options={'ftol': 1e-20} )
        

# Maximize the LLR for m=2, m=3 and m=4 modes.
initial_guess = [1.0, 1.0, 1.0]
res22_33_44 = opt.minimize(utils.NegativeLogLikelihoodRatio, initial_guess, \
              args=[ [Salpha22, Salpha33, Salpha44], Yalpha, covinv, mualpha], \
              method='Powell', options={'ftol': 1e-20} )

# Compute the final statistic \beta
sArr33 = res22_33.x[1]*Salpha33
gammasq33 = np.dot( sArr33, np.dot(covinv, sArr33) )
beta33 = np.dot(Yalpha-res22_33.x[0]*Salpha22-mualpha, np.dot(covinv, sArr33))/gammasq33**0.5

sArr44 = res22_33_44.x[2]*Salpha44
gammasq44 = np.dot( sArr44, np.dot(covinv, sArr44) )
beta44 = np.dot(Yalpha-res22_33_44.x[0]*Salpha22 - res22_33_44.x[1]*Salpha33 - \
               mualpha, np.dot(covinv, sArr44))/gammasq44**0.5

onsoure_data = [ beta33, gammasq33**0.5, beta44,  gammasq44**0.5]

nslices = 12
SalphaList33 = f['noisebg']['SalphaList22'][()]
SalphaList44 = f['noisebg']['SalphaList33'][()]

# Compute the p-value for quantifying the evidence of higher-order modes
# Calculate the distribution p(\beta | H_0) using the off-source data.
# Formula of \beta for off-source data: \beta = < (Y-\mu) | S >/gamma
betaList33, betaList44 = [], []
k = 0
for ii in range(deltaIdx+nslices, Nalpha_dataFull.shape[0]-deltaIdx-nslices):
    # Nalpha samples surrounding the ii-th Nalpha sample, avoid adjacent slices
    Nalpha_data = np.vstack(( Nalpha_dataFull[ii-deltaIdx-nslices: ii-nslices], \
                             Nalpha_dataFull[ii+nslices:  ii+deltaIdx+nslices] ))
    # Mean of N(\alpha)
    mualpha_ii = np.mean(Nalpha_data, axis=0)
    # Compute the covariance matrix of Nalpha
    xy = np.vstack([Nalpha_data[:,j] - mualpha_ii[j] for j in range(len(mualpha_ii))])
    npcov = np.cov(xy)
    covinv = np.linalg.pinv( npcov, rcond=s[args.covmat_rank]/max(s) )
    
    # Use the template which was constructed usning a psd
    # where the psd was estimated using the chunk corresponds to ii-th Nalpha 
    sArr33 = SalphaList33[k]

    sArr44 = SalphaList44[k]
    
    
    gammasq33 = np.dot( sArr33, np.dot(covinv, sArr33) )
    gammasq44 = np.dot( sArr44, np.dot(covinv, sArr44) )
    nArr = Nalpha_dataFull[ii] - mualpha_ii
    b33 = np.dot( nArr, np.dot(covinv, sArr33) )/gammasq33**0.5
    b44 = np.dot( nArr, np.dot(covinv, sArr44) )/gammasq44**0.5
    betaList33.append( b33 )
    betaList44.append( b44 )
    
    k += 1

betaList33 = np.array(betaList33)
betaList44 = np.array(betaList44)

sys.stdout.write("Results for m=3 mode: \n")
sys.stdout.write("LLR(22)= %1.2f \nLLR(22+33)= %1.2f, beta(33)= %1.2f \n" % \
                 (-res22.fun, -res22_33.fun, beta33))

nSamples1 = len(betaList33)
nSamples2 = len( np.where( betaList33 > beta33 )[0])
sys.stdout.write("Total number of off-source samples = %s \n" % nSamples1 )
sys.stdout.write("Number of off-source samples larger than on-source = %s \n" % nSamples2 )

if nSamples2 > 0:
    sys.stdout.write("p-value = %d/%d \n" % (nSamples2, nSamples1))
    pval_3 = nSamples2/nSamples1
else:
    sys.stdout.write("p-value < %d/%d \n" % (1.0, nSamples1))
    pval_3 = 1.0/nSamples1
    
sys.stdout.write("Highest value off-source beta = %s \n\n" % max(betaList33) )




sys.stdout.write("Results for m=4 mode: \n")
sys.stdout.write("LLR(22+33+44)=%1.2f, beta(44)= %1.2f \n"%(-res22_33_44.fun, beta44))
nSamples1 = len(betaList44)
nSamples2 = len( np.where( betaList44 > beta44 )[0])
sys.stdout.write("Total number of off-source samples = %s \n" % nSamples1 )
sys.stdout.write("Number of off-source samples larger than on-source = %s \n" % nSamples2 )
if nSamples2 > 0:
    sys.stdout.write("p-value = %d/%d \n" % (nSamples2, nSamples1))
    pval_4 = nSamples2/nSamples1
else:
    sys.stdout.write("p-value < %d/%d \n" % (1, nSamples1))
    pval_4 = 1.0/nSamples1
    
sys.stdout.write("Number of off-source samples larger than on-source = %s \n" % nSamples2 )
sys.stdout.write("Highest value off-source beta = %s \n" % max(betaList44) )


# Create additional data file for all rank analysis

# SVD of the full covariance matrix
outList = []
for ii in range(1, len(Yalpha), 2 ):
    
    # Compute the inverse of covaraince matrix using reduced SVD,
    covinv = np.linalg.pinv(npcov, rcond= s[ii]/max(s) )

    # Maximize the LLR for signal model m=2 mode.
    initial_guess = [1.0]
    res22 = opt.minimize(utils.NegativeLogLikelihoodRatio, initial_guess, \
                args=[ [Salpha22], Yalpha, covinv, mualpha], method='Powell', \
                options={'ftol': 1e-20} )

    # Maximize the LLR for m=2 and m=3 modes.
    initial_guess = [1.0, 1.0]
    res22_33 = opt.minimize(utils.NegativeLogLikelihoodRatio, initial_guess, \
                       args=[ [Salpha22, Salpha33], Yalpha, covinv, mualpha], \
                       method='Powell', options={'ftol': 1e-20} )

    # Compute the final statistic \beta
    sArr33 = res22_33.x[1]*Salpha33
    gammasq33 = np.dot( sArr33, np.dot(covinv, sArr33) )
    beta33 = np.dot(Yalpha-res22_33.x[0]*Salpha22-mualpha, np.dot(covinv, sArr33))/gammasq33**0.5
    
    outList.append( [ii, beta33, res22.fun, res22_33.fun, res22_33.x[0], res22_33.x[1] ] )

outList = np.array( outList )


# Write the outputs from the analysis
hf = h5py.File( args.output_file, 'w')

# Write the output from fix rank analysis
out = hf.create_group('fix_rank_analysis')
out.create_dataset('covmat_rank',  data=args.covmat_rank )
out.create_dataset('covmat_norm', data = res_cov_norm)
out.create_dataset('residual_norm', data = res_cov_norm)
out.create_dataset('LLR22',     data=-res22.fun)
out.create_dataset('LLR22_33',    data=float(-res22_33.fun))
out.create_dataset('LLR22_33_44', data=float(-res22_33_44.fun))
out.create_dataset('beta33',   data= onsoure_data[0] )
out.create_dataset('gamma33',   data= onsoure_data[1] )
out.create_dataset('beta44',   data=onsoure_data[2] )
out.create_dataset('gamma44',   data= onsoure_data[3] )
out.create_dataset('pvalue33',   data=float(pval_3))
out.create_dataset('pvalue44',   data=float(pval_4))
out.create_dataset('beta33H0', data=betaList33)
out.create_dataset('beta44H0', data=betaList44)


# Write the output from all rank analysis
outall = hf.create_group('all_rank_analysis')
outall.create_dataset('rank', data= outList[:,0] )
outall.create_dataset('beta33', data= outList[:,1] )
outall.create_dataset('LLR22', data= outList[:,2] )
outall.create_dataset('LLR22_33', data= outList[:,3] )
outall.create_dataset('amp2', data= outList[:,4] )
outall.create_dataset('amp3', data= outList[:,5] )

hf.close()


