from __future__ import division
import numpy as np
import scipy as sp
import pycwt
import h5py
import lal
import lalsimulation as lalsim
from ligo.gracedb.rest import GraceDb
import pycbc
import pycbc.psd


__author__ = "Soumen Roy <soumen.roy@ligo.org>"


def NegativeLogLikelihoodRatio(params, args):
    """
    This function is a helper function for 'scipy.minimize' to maximize the LLR
    over unknown overall amplitudes of the template model of each 'm' mode. 
    It calculates the LLR for a given set of unknown amplitudes.
    
    Parameters
    ----------
    params: list
        List of the unknown amplitudes.
    args: list
        List of the Salpha, Yalpha, Inverse of the covarainace matrix, mualpha
    
    Return 
    -------
        It returns '-LLR' if all the unknown amplitudes are positive.
        
    """
    # Set a constraint for the unknown amplitude parameters
    if min(params)<0:
        return 10000000
    else:
        sArr, yArr, covinv, muArr = args
        # combined template
        msArr = np.sum(np.array( [params[i]*sArr[i] for i in range(len(params))] ), axis=0)
        # square of the template norm
        gammasq = np.dot( msArr, np.dot(covinv, msArr) )
        # compute LLR
        LLR = (np.dot( yArr-muArr, np.dot(covinv, msArr) )- 0.5 * gammasq)
        return -LLR
    

waveforms_list = ["EOBNRv2HM", "SEOBNRv4HM", "SEOBNRv4PHM", "IMRPhenomHM", "IMRPHenomPv3HM"] 


class MiscellaneousUtils(object):
    """
    This Class conatains the miscellaneous utils for generating the waveform using LALSimulation,
    time-frquency path, summing the energies along the scaled tracks.
    """
    
    _LAL_DICT_PARAMS = {"Lambda1": "lambda1", "Lambda2": "lambda2", "ampO": "ampO", "phaseO": "phaseO"}
    _LAL_DICT_PTYPE = {"Lambda1": lal.DictInsertREAL8Value, "Lambda2": lal.DictInsertREAL8Value, "ampO": lal.DictInsertINT4Value, "phaseO": lal.DictInsertINT4Value}
    
    
    def __init__(self, phiref=0., deltaT=1./4096., mass1=10.*lal.MSUN_SI,
            mass2=10.*lal.MSUN_SI, spin1x=0., spin1y=0., spin1z=0.,
            spin2x=0., spin2y=0., spin2z=0., fmin=15., fref=0., dist=1.e6*lal.PC_SI*300,
            iota=lal.PI/4, lambda1=0., lambda2=0., waveFlags=None, nonGRparams=None,
            ampO=-1, phaseO=-1, approx="SEOBNRv4HM", 
            noise_model=None,
            theta=0., phi=0., psi=0., tref=0., radec=False, detector="H1", event_time=1134604817,
            deltaF=0.2, fmax=2048., nr_hdf5_filepath=None):
        
        self.phiref = phiref
        self.deltaT = deltaT
        self.mass1 = mass1
        self.mass2 = mass2
        self.spin1x = spin1x
        self.spin1y = spin1y
        self.spin1z = spin1z
        self.spin2x = spin2x
        self.spin2y = spin2y
        self.spin2z = spin2z
        self.fmin = fmin
        self.fref = fref
        self.dist = dist
        self.iota = iota
        self.lambda1 = lambda1
        self.lambda2 = lambda2
        self.waveFlags = waveFlags
        self.nonGRparams = nonGRparams
        self.ampO = ampO
        self.phaseO = phaseO
        self.approx = approx
        self.theta = theta     # DEC.  DEC =0 on the equator; the south pole has DEC = - pi/2
        self.phi = phi         # RA.   
        self.psi = psi
        # FIXME: make into property
        self.longAscNodes = 0.0
        # FIXME: Allow to be a value at some point
        self.eccentricity = 0.0
        self.meanPerAno = 0.0
        # Event time at geocentric coordinate
        self.event_time = event_time
        # Event time at detector coordinate
        self.det_event_time = event_time
        self.tref = tref
        self.radec = radec
        self.detector = detector
        self.deltaF=deltaF
        self.fmax=fmax
        
        self.nr_hdf5_filepath = nr_hdf5_filepath
        
        self.noise_model = noise_model
        self.frame_type = None
        self.channel_name = None
        self.chunk_length = None
        self.mode_list = None
        
        self.alpha_arr = None
        self.track_max_length = None
        self.track_end_frequency = None
        
        # Parameters for CWT
        self.cwt_fhigh = None
        # Admissibility constant
        self.wavelet_Cg = None
        
        self.gracedb_id = None
        
        
    def to_lal_dict(self):
        """
        This funtion returns LAL dictonary object which contains
        the tidal parameters, amplitude order, phase order, and
        GR corrections parameters.
        """
        extra_params = lal.CreateDict()
        for k, p in MiscellaneousUtils._LAL_DICT_PARAMS.iteritems():
            typfunc = MiscellaneousUtils._LAL_DICT_PTYPE[k]
            typfunc(extra_params, k, getattr(self, p))
        
        if self.nonGRparams != None:
            for key in self.nonGRparams.keys():
                f = getattr(lalsim, 'SimInspiralWaveformParamsInsertNonGRDChi'+ key[-1])
                f(extra_params, self.nonGRparams[key]) 
            
            
        # Properly add tidal parammeters
        lalsim.SimInspiralWaveformParamsInsertTidalLambda1(extra_params, self.lambda1)
        lalsim.SimInspiralWaveformParamsInsertTidalLambda2(extra_params, self.lambda2)
        
        # Activate the specified modes
        if self.mode_list is not None:
            ma = lalsim.SimInspiralCreateModeArray()
            for l, m in self.mode_list:
                lalsim.SimInspiralModeArrayActivateMode(ma, l, m)
            lalsim.SimInspiralWaveformParamsInsertModeArray(extra_params, ma)
            
        if self.approx == 'NR_hdf5':
            f = h5py.File(self.nr_hdf5_filepath, 'r')
            lalsim.SimInspiralWaveformParamsInsertNumRelData(extra_params, self.nr_hdf5_filepath)
            m1, m2 = f.attrs['mass1'], f.attrs['mass2']
            
            spins = lalsim.SimInspiralNRWaveformGetSpinsFromHDF5File(-1, 1, self.nr_hdf5_filepath)
            self.spin1x = spins[0]
            self.spin1y = spins[1]
            self.spin1z = spins[2]
            self.spin2x = spins[3]
            self.spin2y = spins[4]
            self.spin2z = spins[5]
            f_lower = f.attrs['f_lower_at_1MSUN']
            self.fmin = f_lower/(self.mass1+self.mass2)
            mtotal = self.mass1+self.mass2
            
            self.mass1, self.mass2 = mtotal * m1/(m1+m2), mtotal * m2/(m1+m2)
            f.close()
        return extra_params
    
    

    
    def _whiten(self, strain, psd):
        """
        Whitened the time series for a given power spectral density of noise.
        
        The output normalised such that, if the raw strain is stationary and
        Gaussian, then the output will have zero mean and unit variance.
        
        strain: pycbc.types.TimeSeries
            Raw strain data.
        
        psd: pycbc.type.FrequencySeries
            Power spectral density.
        """
        Nt = len(strain)
    
        freqs = np.fft.rfftfreq(Nt, strain.delta_t)
        # whitening: transform to freq domain, divide by asd, then transform back, 
        # taking care to get normalization right.
        hf = np.fft.rfft(strain.data)
        
        white_hf = hf / (np.sqrt(psd.data /strain.delta_t/2))
        white_ht = np.fft.irfft(white_hf, n=Nt)
        return pycbc.types.TimeSeries(white_ht, delta_t=strain.delta_t)
    
    
    def _project_hplus_hcross(self, hplus, hcross):
        """
        Compute antenna factors Fplus and Fcross anmd project the polarizations
        in the detector frame.
        """
        detector = lal.cached_detector_by_prefix[self.detector]
        Fp, Fc = lal.ComputeDetAMResponse(detector.response, self.phi, \
                                    self.theta, self.psi, lal.GreenwichMeanSiderealTime(self.event_time))

        # form strain signal in detector
        hplus.data.data = Fp*hplus.data.data + Fc*hcross.data.data

        return hplus
    
    def _time_delay_from_geocenter(self, detector=None, ra=None, dec=None, gpstime=None):
        """
        Calculate the time delay (UNIT: Sec.) between the geocentre and a given detector
        for a signal from a given sky location of the source.
        
        Parameters
        ----------
        detector: str
            A string describing the detector, e.g. L1 
        ra: float
            The right-ascension of the source. UNIT: radians
        dec: float
            The declination of the source. UNIT: radians
        
        """
        
        if not any([ detector, ra, dec, gpstime ] ) == True:
            
            return lal.TimeDelayFromEarthCenter( \
                        lalsim.DetectorPrefixToLALDetector(self.detector ).location, \
                        self.phi, self.theta, self.event_time)
        else:
            return lal.TimeDelayFromEarthCenter( \
                        lalsim.DetectorPrefixToLALDetector(detector ).location, \
                        ra, dec, gpstime)
    
    
    def _gen_tukey_window(self, length, beta=0.05, start=True, end=True):
        """
        This function generates a Tukey window, use it in the Fourier transform
        to avoid spectral leakage due to the sharp cut of time-domain waveform/data.
        """

        window = lal.CreateTukeyREAL4Window(length, beta).data.data[:]
        if start is False:
            window[ :length//2 ] = 1.0
        elif end is False:
            window[ length//2: ] = 1.0
        return window
            
    
    def _gen_waveform(self, polarizations=False, time_domain=False, frequency_domain=False):
        
        """
        Generate time/frequency domain waveform from LALSimulation.
        
        parameters
        ----------
        polarizations: bool
            Set polarizations True to get the plus and cross polarizations.
            default is False, OPTIONAL.
        
        time_domain: bool
            Set time_domain True to get only time domain waveform even if
            the specified waveform model exists in frequency domain.
            defalult False, OPTIONAL.
            
        frequency_domain: bool
            Set frequency_domain True to get only frequency domain waveform even if
            the specified waveform model exists in time domain.
            defalult False, OPTIONAL.
        """
        
        approximant = lalsim.GetApproximantFromString( self.approx )
        extra_params = self.to_lal_dict()
        
        if lalsim.SimInspiralImplementedFDApproximants(approximant):
            if time_domain is False:
               
                hp, hc = lalsim.SimInspiralChooseFDWaveform(self.mass1*lal.MSUN_SI, self.mass2*lal.MSUN_SI, \
                         self.spin1x, self.spin1y, self.spin1z, self.spin2x, self.spin2y, self.spin2z, \
                         self.dist, self.iota, self.phiref, self.longAscNodes, self.eccentricity, \
                         self.meanPerAno, self.deltaF, self.fmin, self.fmax, self.fref, extra_params, approximant)
            else:
                hp, hc = lalsim.SimInspiralTD(self.mass1*lal.MSUN_SI, self.mass2*lal.MSUN_SI,
                         self.spin1x, self.spin1y, self.spin1z, self.spin2x, self.spin2y, self.spin2z, \
                         self.dist, self.iota, self.phiref, self.longAscNodes, self.eccentricity, \
                         self.meanPerAno, self.deltaT, self.fmin, self.fref, extra_params, approximant)

        elif lalsim.SimInspiralImplementedTDApproximants(approximant):
            hp, hc = lalsim.SimInspiralChooseTDWaveform(self.mass1*lal.MSUN_SI, self.mass2*lal.MSUN_SI,
                     self.spin1x, self.spin1y, self.spin1z, self.spin2x, self.spin2y, self.spin2z, \
                     self.dist, self.iota, self.phiref, self.longAscNodes, self.eccentricity, \
                     self.meanPerAno, self.deltaT, self.fmin, self.fref, extra_params, approximant)
            
            if frequency_domain is True:
                window = self._gen_tukey_window(hp.data.length, end=False)
                hp = pycbc.types.TimeSeries( hp.data.data[:]*window, delta_t=hp.deltaT )
                hc = pycbc.types.TimeSeries( hc.data.data[:]*window, delta_t=hc.deltaT )
                
                hp = hp.to_frequencyseries().lal()
                hc = hc.to_frequencyseries().lal()
                
        else:
                raise ValueError("Must specify valid waveform model approximant")
        
        if polarizations is False:
            return self._project_hplus_hcross(hp, hc)
        else:
            return hp, hc
        
        
    
    

    def _get_fpeak(self):
        """
        Calculate the frequency of the GW at the time of coalescence.
        """
        # Calculate the peak frequency (2, 2) mode for aligned-spin systme
        if all( s == 0 for s in [self.spin1x, self.spin1y, self.spin2x, self.spin2y]):
            if self.approx.startswith('SEOBNR'): 
                freqFunc = getattr(lalsim, "f%sPeak"%self.approx[:8] )
            elif self.approx.startswith('IMRPhenom'):
                freqFunc = getattr(lalsim, "fIMRPhenomDPeak")
                
            f_peak = lalsim.SimInspiralGetFrequency(self.mass1*lal.MSUN_SI, self.mass2*lal.MSUN_SI, \
                0.0, 0.0, self.spin1z, 0.0, 0.0,  self.spin2z, freqFunc)
            
        # For precessing spin waveform, calculate the peak frequency
        # in the coprecessing frame, example: IMRPhenomPv2 waveform in the coprecessing frame is just the
        # IMRPhenomD waveform with the aligned spin components
        else:
            if self.approx.startswith('IMRPhenomP'):
                
                chi1_l, chi2_l, chip, thetaJN, alpha0, phi_aligned, zeta_polariz = \
                    lalsim.SimIMRPhenomPCalculateModelParametersFromSourceFrame( \
                    self.mass1*lal.MSUN_SI, self.mass2*lal.MSUN_SI, self.fref, self.phiref, self.iota, \
                    self.spin1x, self.spin1y, self.spin1z, self.spin2x, self.spin2y, self.spin2z, lalsim.IMRPhenomPv2_V)
                
                freqFunc = getattr(lalsim, "fIMRPhenomDPeak")
                f_peak = lalsim.SimInspiralGetFrequency(self.mass1*lal.MSUN_SI, self.mass2*lal.MSUN_SI, \
                             0.0, 0.0, chi1_l, 0.0, 0.0, chi2_l, freqFunc)
            
        return f_peak

    
    def _gen_SEOBNRv4P_time_frequency(self, fstart):
        """
        This function creates the time-frequency track of 22-mode using the EOB dynamics.
        For the precessing spin case, it assumes the initial spin configuration at a
        specified starting frequency 'fstart'.
        """
        # Empty LALParams dict
        LALParams = lal.CreateDict();
        
        modearray = lalsim.SimInspiralCreateModeArray()
        lalsim.SimInspiralModeArrayActivateMode(modearray, 2, 2)
        
        seobflags = lal.CreateDict();
        lal.DictInsertINT4Value(seobflags, "SEOBNRv4P_SpinAlignedEOBversion", 4)
        NumericalOrAnalyticalHamiltonianDerivative = \
                        lalsim.SimInspiralWaveformParamsLookupEOBChooseNumOrAnalHamDer(LALParams)
        

        deltaT = 1.0/16384.0
        inclination = self.iota
        phi_ref = self.phiref
        hplus, hcross, hIlm, hJlm, dyn_Low, dyn_Hi, dyn_all, t_vec_modes, \
                hP22_amp, hP22_phase, hP21_amp, hP21_phase, hP33_amp, \
                hP33_phase, hP44_amp, hP44_phase, hP55_amp, hP55_phase, \
                alphaJ2P, betaJ2P, gammaJ2P, AttachPars = \
                lalsim.SimIMRSpinPrecEOBWaveformAll(phi_ref, deltaT, self.mass1*lal.MSUN_SI, \
                    self.mass2*lal.MSUN_SI, fstart, self.dist, inclination, \
                    self.spin1x, self.spin1y, self.spin1z,  self.spin2x, self.spin2y, self.spin2z, \
                    modearray, seobflags)
        
        # The output here is a single REAL8Vector, to be reshaped into the format:
        # t, x, y, z, px, py, pz, s1x, s1y, s1z, s2x, s2y, s2z, phiD, phi, vx, vy, vz, r, phi, pr, pphi, Omega, s1dotZ, s2dotZ, H
        # Spins in units of M^2
        nptdyn = len(dyn_all.data)//26
        dyn_arr = np.reshape(dyn_all.data, (26, nptdyn))
        t = dyn_arr[0]
        # Orbital frequency
        Omega = dyn_arr[22]
        
        # time of peak of Omega:  tPeakOmega
        t_peak = AttachPars.data[0]*(self.mass1+self.mass2)*lal.MTSUN_SI
        
       
        times = t_peak - t*(self.mass1+self.mass2)*lal.MTSUN_SI 
        # 22-mode frequency equal to 2*Omega 
        freqs = Omega/(self.mass1+self.mass2)/lal.MTSUN_SI/lal.PI
        return times, freqs


    def _gen_time_frequency_path(self, start_freq=None, track_max_length=None, track_end_frequency=None):
        """
        This function generates the time-frequency path [ f_22(t; m1, m2, s1z, s2z), 
        t_22(f; m1, m2, s1z, s2z)] of the (2, 2) mode for a given frquency domain waveform model.
        The time-frquency track is computred from the frequency derivative of the FD phase,
        2pi t(f) =  dPsi/df.
        
        
        paramsters:
        ----------
        
        start_freq: float
            Lower bound on the starting frequency of the (2, 2) mode. UNIT: Hz
            
        track_max_length: float
            Maximum length of the track which measured by starting from a fiducial
            frequency to coalescence. Please remember that upper frequency limit
            of the (2, 2) is f_ISCO. UNIT: Sec
            
        track_end_frequency: float
            Ending frequency of the (2, 2) mode track. UNIT: Hz
            
        Returns:
        --------
        
        tArrFixDT, fArrFixDT: numpy.array
            tArrFixDT and fArrFixDT are time and frequency samples of f(t) curve, 
            where tArrFixDT is an array of evenly spaced time samples with a step of self.deltaT.
        
        interpF: Class object
            scipy.interpolate.interp1d object of interpolated function 'f = f(t)'.
            
        interpT: Class object
            scipy.interpolate.interp1d object of interpolated function 't = t(f)'.
            
        """
        if start_freq < self.fmin:
            raise ValueError("start_freq must greater equal to fmin of the analysis")
        
        if self.approx.startswith('SEOBNRv4P'):
            inst_time, inst_freq = self._gen_SEOBNRv4P_time_frequency(self.fmin)
        else:
            
            f_peak = self._get_fpeak()
        
            # Reset the values of lower cutoff frequency, highest frequency and 
            # frequency sampling to generate a longer waveform with larger sampling rate.
            # It is necessary to increase the accuracy in the iterpolation for 'f = f(t)' function.
            params_vals = [self.deltaF, self.fmin, self.fmax, self.approx]
            
            if self.approx in ['SEOBNRv2HM', 'SEOBNRv4HM']:
                self.approx = self.approx[:8] + '_ROM'
            
            # Generate the frequency domain waveform
            self.mode_list = [(2, 2)]
            self.fmin = 5.0
            if lalsim.SimInspiralImplementedFDApproximants( getattr(lalsim, self.approx) ):
                self.deltaF = 0.001
                # Generate two polarizations of the GW
            hp, hc = self._gen_waveform(polarizations=True, frequency_domain=True)
            self.deltaF = hp.deltaF
            
            self.fmin = 10.0
                
            
            self.fmax = int(f_peak)+2.0
            # Instantaneous frequency domain phase (Psi) 
            fmin_indx = int(self.fmin/self.deltaF)
            fmax_indx = int(self.fmax/self.deltaF + 1)
            inst_phase = np.unwrap( np.angle(hp.data.data[fmin_indx:fmax_indx + 1]) ) 
            
            # Curve of instantaneous time t(f): 2 \pi t(f) = dPsi/df
            inst_time = np.diff( inst_phase )/(self.deltaF*2*lal.PI)
            inst_freq = np.array( [self.fmin+ii*self.deltaF for ii in range( len(inst_time) )])
            #inst_freq = np.arange( self.fmin, self.fmax+self.deltaF, self.deltaF)

            # Time correction is t(f_peak) = 1/(2pi) dPsi/df (f_peak)
            t_corr =  inst_time[ int(  (f_peak-self.fmin)/self.deltaF ) ] 
            inst_time -= t_corr 
            
            
            # Reset the previous values
            self.deltaF, self.fmin, self.fmax, self.approx = params_vals

        # Interpolated function 'f = f(t)' and 't = t(f)',
        # where both the track assume the coalescence time at t=0
        # i.e. t(f_peak) = 0
        interpF = sp.interpolate.interp1d(inst_time, inst_freq)
        interpT = sp.interpolate.interp1d(inst_freq, inst_time)
        
        # Find the required start_time and end_time of the track.
        if track_max_length is None:
            track_max_length = self.track_max_length     
        if start_freq is None and  track_max_length is None:
            t_start = interpT(self.fmin)
        elif start_freq is None and  track_max_length is not None:
            t_start = track_max_length
        else:
            t_start = min( interpT(start_freq), track_max_length )
        if track_end_frequency is None:
            track_end_frequency = self.track_end_frequency
        t_end = int(interpT(track_end_frequency)/self.deltaT)*self.deltaT
        
        
        # Generate the time samples and frequency samples for (2, 2) mode
        timeArr = np.arange(t_start, t_end, -self.deltaT )
        freqArr = interpF(timeArr)
        
        return self.tref-timeArr, freqArr, interpF, interpT
    

    def _convert_tfpath_to_index(self, alpha, tArrFixDT, fArrFixDT, interpT):
        """
        This function privides indices of the track on the time-frequency map 
        by using the scaled track alpha*f_22(t)
        
        Parameters:
        -----------
        alpha: float
            Scale parameter
        
        tArrFixDT, fArrFixDT: numpy.array
            tArrFixDT and fArrFixDT are time and frequency samples of f(t) curve, 
            where tArrFixDT is an array of evenly spaced time samples with a step of self.deltaT.
        
        interpT: Class object
            scipy.interpolate.interp1d object of f(t).
        
        """
        farr = alpha*fArrFixDT
        tindices = np.array(tArrFixDT/self.deltaT).astype(int).tolist()
        findices = np.array(farr/self.deltaF).astype(int).tolist()
        for i in range(1, len(tArrFixDT)):
            curr_freq = farr[i-1]

            # In the late inspiral regime, the f(t) curve can increase rapidly.
            # In this case, the change in frequency within a time-step of deltaT
            # [ f(t+\deltaT)-f(t) ] may be greater than \deltaF.
            # Thus, we can miss a few pixels on the right track.
            # We incorporate them here from the t(f) curve.
            while  int((farr[i]- curr_freq)/self.deltaF) > 1:
                curr_freq += self.deltaF 
                curr_time = self.tref - interpT(curr_freq/alpha)
                tindices.append(int(curr_time/self.deltaT))
                findices.append(int(curr_freq/self.deltaF))
        return np.array(tindices), np.array(findices)
    

    
    
    
    def _stack_pixels_energies(self, tfMap, tArrFixDT, fArrFixDT, intpT):
        
        """
        This function is summing the pixels energies along the scaled tracks:
        Ref: Equation 4 in arXiv:1910.04565, Link: https://arxiv.org/pdf/1910.04565.pdf#equation.0.4
        
        Parameters:
        -----------
        tfMap: numpy.array
            Time-frequency map.
            
        tArrFixDT, fArrFixDT: numpy.array
            tArrFixDT and fArrFixDT are time and frequency samples of f(t) curve, 
            where tArrFixDT is an array of evenly spaced time samples with a step of self.deltaT.
        
        intpT: Class object
            scipy.interpolate.interp1d object of t(f).
            
        """
        stack_eng = []
        for alpha in self.alpha_arr:
            # Indicies of the scaled tracks, scale parameter is alpha
            time_index, freq_index =  self._convert_tfpath_to_index( \
                    alpha, tArrFixDT, fArrFixDT, intpT)
            
            # Sum the energies of the pixles along the scaled track.
            stack_eng.append( np.sum(tfMap[freq_index-int(1.0/self.deltaF), time_index]))

        return  np.array(stack_eng)
    
    
    def _gen_template_model(self, psd, tArrFixDT, fArrFixDT, intpT):
        
        """
        This function generates the template model from time-frequency map of the
        time-domain waveform.
        
        Ref: Equation 4 in arXiv:1910.04565, Link: https://arxiv.org/pdf/1910.04565.pdf#equation.0.4
        
        parameters:
        -----------
        
        psd: pycbc.types.FrequencySeries
            We whiten the time-domain using a PSD obtained from a strain
            surroundings the event.
            
        tArrFixDT, fArrFixDT: numpy.array
            tArrFixDT and fArrFixDT are time and frequency samples of f(t) curve, 
            where tArrFixDT is an array of evenly spaced time samples with a step of self.deltaT.
        
        intpT: Class object
            scipy.interpolate.interp1d object of t(f).
            
        Returns:
        --------
        S_alpha: numpy.array
            template model.
        
        power: numpy.array
            Absolute square of the complex time-frequency representation of the 
            time domain waveform.
        wave: numpy.array
            Complex time-frequency representation of the time domain waveform.
        
        """
        
        # Generate the waveform with specified mode and the (2, 2) mode waveform.
        # There may be a time difference between modes amplitude
        # peaks. The analysis assumes the coalescence time of  (2, 2) mode  is
        # at tref. A time shift is necessary for non-quadrupole modes.
        mode_list = self.mode_list
   
        if self.approx == 'SEOBNRv4PHM':
            # Empty LALParams dict
            LALParams = lal.CreateDict();
            modearray = lalsim.SimInspiralCreateModeArray()
            lalsim.SimInspiralModeArrayActivateMode(modearray, 2, 2)
        
            seobflags = lal.CreateDict();
            lal.DictInsertINT4Value(seobflags, "SEOBNRv4P_SpinAlignedEOBversion", 4)
            NumericalOrAnalyticalHamiltonianDerivative = \
                        lalsim.SimInspiralWaveformParamsLookupEOBChooseNumOrAnalHamDer(LALParams)
        
            deltaT = 1.0/16384.0
            _all_output = lalsim.SimIMRSpinPrecEOBWaveformAll(self.phiref, deltaT, self.mass1*lal.MSUN_SI, \
                    self.mass2*lal.MSUN_SI, self.fmin, self.dist, self.iota, \
                    self.spin1x, self.spin1y, self.spin1z,  self.spin2x, self.spin2y, self.spin2z, \
                    modearray, seobflags)
        
            # time of peak of Omega:  tPeakOmega
            AttachPars = _all_output[-1]
            t_peak = AttachPars.data[0]*(self.mass1+self.mass2)*lal.MTSUN_SI
        
            
            if (2, 2) in self.mode_list:
                wf = self._gen_waveform()
            else:
                self.mode_list = [(2, 2)]
                h22 = self._gen_waveform()
                
                self.mode_list.extend( mode_list )
                wf = self._gen_waveform()
                wf.data.data[:] -= h22.data.data[:] 
            
            # Convert the waveform from lal.REALTimeSeries to pycbc.TimeSeries with a correct length
            if t_peak > self.tref-0.2:
                min_idx = int( (t_peak - self.tref - 0.2)/self.deltaT)
                t_peak = self.tref - 0.2
            else:
                min_idx = 0
            wf = pycbc.types.TimeSeries(wf.data.data[min_idx:], delta_t=self.deltaT)
            
            # Resize the waveform to a length (TimeDuration*SamplingFrequency)
            length = int(1.0/self.deltaF/self.deltaT)
            wf.resize(length)

            # Multiply by Tukey window to taper at the start point.
            wf.data *= self._gen_tukey_window(length, beta=0.05, start=True, end=False)
            
            # Set the peak index at self.tref
            peak_indx = int(t_peak/self.deltaT)
            col_indx = int( self.tref/self.deltaT)
            shift_indx = col_indx-peak_indx
            wf.roll(shift_indx)
            
        else:
            if self.approx in ['EOBNRv2HM', 'SEOBNRv4HM', 'NR_hdf5']:
                wf = self._gen_waveform()
                self.mode_list = [(2, 2)]
                h22 = self._gen_waveform()

                # Convert the waveform from lal.REALTimeSeries to pycbc.TimeSeries
                wf = pycbc.types.TimeSeries(wf.data.data[:], delta_t=self.deltaT)
                h22 = pycbc.types.TimeSeries(h22.data.data[:], delta_t=self.deltaT)
                
            elif self.approx in ['IMRPhenomHM', 'IMRPhenomPv3HM']:
                self.mode_list = [(2, 2)]
                h22 = self._gen_waveform(time_domain=True)

                if self.approx == 'IMRPhenomHM':
                    self.mode_list = mode_list
                    wf = self._gen_waveform(time_domain=True)
                else:
                    if mode_list == [(2, 2)]:
                        wf = self._gen_waveform()
                    else:
                        self.mode_list.extend( mode_list )
                        wf = self._gen_waveform()
                        wf.data.data[:] -= h22.data.data[:] 

                startIdx = int(intpT(self.fmin)/self.deltaT)
                maxIdx_h22 = np.where(np.abs(h22.data.data[:]) == np.max(np.abs(h22.data.data[:])))[0][0]
                maxIdx_wf = np.where(np.abs(wf.data.data[:]) == np.max(np.abs(wf.data.data[:])))[0][0]


                # Convert the waveform from lal.REALTimeSeries to pycbc.TimeSeries
                length = startIdx+wf.data.length-maxIdx_wf
                window = self._gen_tukey_window(length, beta=0.05, end=False)
                wf = pycbc.types.TimeSeries(window*wf.data.data[-length:], delta_t=self.deltaT)
                h22 = pycbc.types.TimeSeries(window*h22.data.data[-length:], delta_t=self.deltaT)
            
            
            # Resize the waveform to a length (TimeDuration*SamplingFrequency)
            length = int(1.0/self.deltaF/self.deltaT)
            wf.resize(length)
            h22.resize(length)

            # Multiply by Tukey window to taper at the start point.
            wf.data *= self._gen_tukey_window(length, beta=0.05, start=True, end=False)

            # Set the peak index at self.tref
            peak_indx = h22.abs_arg_max()
            col_indx = int( self.tref/self.deltaT)
            shift_indx = col_indx-peak_indx
            wf.roll(shift_indx)
        
        # Interpolate the PSD if the required length is not equal 
        # to the 1/(frequency sampling of PSD).
        if len(psd) != wf.shape[0]//2 + 1:
            psd = pycbc.psd.interpolate(psd, self.deltaF)
        else: pass
        
        # Whiten the time domain waveform
        whiten_wf = self._whiten(wf, psd)

        
        # Compute the CWT
        wave =  self._compute_cwt(whiten_wf.data, norm=True)[0] 
        power = np.abs(wave)**2.0
        
        # Construct the template model
        S_alpha = self._stack_pixels_energies( power, tArrFixDT, fArrFixDT, intpT)
        
        return S_alpha,  wf, power, wave
    
    
    def _set_gracedb_params(self, gracedb_id, det=None):
        """
        Extract the GraceDB parameters from the given GraceDB event id.
        Do ligo-proxy-init before running the script.
        
        gracedb_id:
             GraceDB G-event id.
        """
        if det != None:
            self.detector = det
        client = GraceDb()
        response = client.event(gracedb_id)
        fres = response.json()
        
        self.event_time = fres['gpstime']
        
        for ii in range( len(fres['extra_attributes']['SingleInspiral'])):
            if fres['extra_attributes']['SingleInspiral'][ii]['ifo'] == self.detector:
                params = fres['extra_attributes']['SingleInspiral'][ii]
                
        # Set the parameters
        self.mass1, self.mass2, self.spin1z, self.spin2z, self.phiref, self.det_event_time = \
                    params['mass1'], params['mass2'], params['spin1z'], params['spin2z'], \
                    params['coa_phase'], params['end_time']+params['end_time_ns']/1e9
        
        
    def _set_params_from_summary_of_posterior_samples(self, summary_sts_file, _type='maxL'):
        
        # Set the intrinsic parameters and event time obetained from detection pipeline
        datContent = [i.strip().replace('#','').split() for i in open(summary_sts_file).readlines()]
        header = datContent[0]
        params = [line[0] for line in datContent] 
        
        func = lambda P, T: float( datContent[params.index(P)][ header.index(_type)+1] )
       
        self.mass1 =  func( 'm1', _type )
        self.mass2 =  func( 'm2', _type )
        self.spin1z = func( 'a1z', _type )
        self.spin2z = func( 'a2z', _type )

        self.dist = func( 'dist', _type )*lal.PC_SI*1e6
        self.theta = func( 'dec', _type )
        self.phi = func( 'ra', _type )
        self.psi = func( 'psi', _type )
        self.iota = np.arccos( func( 'costheta_jn', _type ) )
        self.phiref = func('phase', _type)
        self.fmin = func('flow', _type)
        self.fref = func('f_ref', _type)
        
        # Try to set precessing spins parameters if available
        try: 
            self.iota, self.spin1x, self.spin1y, self.spin1z, self.spin2x, self.spin2y, self.spin2z = \
                 lalsim.SimInspiralTransformPrecessingNewInitialConditions(np.arccos( func( 'costheta_jn', _type ) ), \
                  func('phi_jl', _type), func('tilt1', _type), func('tilt2', _type), func('phi12', _type), \
                    func('a1', _type), func('a2', _type), self.mass1*lal.MSUN_SI, self.mass2*lal.MSUN_SI, \
                    func('f_ref', _type), func('phase', _type))
        
        except: pass
            
        try:
            self.event_time  = func( 'time', _type ) 
        except: 
            self.event_time  = func( 't0', _type )
        
        self.det_event_time = self.event_time + self._time_delay_from_geocenter()
        
    def _compute_cwt(self, ts, ndm_freq=None, cwt_fhigh=None, deltaT=None, norm=False):
        """
        Compute the CWT from a time series. The CWT use the Morlet wavelet.
        
        Parameters
        ----------
        ts: numpy.array
            Time series. UNIT: Sec
        ndm_freq: float
            Non-dimensional frequency of Morlet wavelet.
        cwt_fhigh: float
            Maximum frequency of time-frequency map from CWT. UNIT: Hz
        deltaT: float
            Sampling rate in the time-series.
            
        """
        
        if ndm_freq == None:
            ndm_freq = self.ndm_freq
        if cwt_fhigh == None:
            cwt_fhigh = self.cwt_fhigh
        if deltaT == None:
            deltaT = self.deltaT
            deltaF = self.deltaF
        else:
            deltaF = 1.0/len(ts)/deltaT

        freqs = np.arange(1, cwt_fhigh+deltaF, deltaF)
        
        # Construct a Morlet wavelet
        mother = pycwt.Morlet(ndm_freq)
        
        # Construct the time frequency map
        wave, scales, freqs, coi, fft, fftfreqs = pycwt.cwt(ts, deltaT, freqs=freqs, wavelet=mother)
        
        if norm is True:
            da = -np.gradient(scales)
            fc = (self.deltaT*da/self.wavelet_Cg)**0.5/scales
            wave =  wave*fc[:,None]
        else:
            pass
       
        return wave, scales, freqs, coi, fftfreqs
            
        
        
        
