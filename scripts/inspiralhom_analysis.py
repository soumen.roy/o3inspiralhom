"""
This program analyses the single detector data for the detection of inspiral
higher-order modes from a detected event. The analysis uses the summary file of
the posterior distribution obtained from parameter estimation study or GraceDB
G-event id or specifying the component masses, dimensionless spins, event time.
Finally, the program write out the results and plots of the time-frequency map
and alpha-map.

Reference: https://arxiv.org/abs/1910.04565

"""

from __future__ import division

import argparse
from ConfigParser import ConfigParser
import os, sys
import logging
import numpy as np
import scipy as sp
import scipy.special as spl
import scipy.stats as sts
import scipy.optimize as opt
import h5py
import pycwt

import lal
import pycbc.frame
import pycbc.psd 
from pycbc.filter import resample_to_delta_t

import noisebg
from calndm import FindNonDimensionalFrequency
import utils
mUtils = utils.MiscellaneousUtils()

__author__ = "Soumen Roy <soumen.roy@ligo.org>"
__program__ = "inspiralhom_analysis"
__version__ = "no version"
usage = """ 

Using Posterior summary file:
inspiralhom_analysis --detector L1 \\
            --frame-type L1_HOFT_C00 \\
            --channel-name L1:GDS-CALIB_STRAIN \\
            --window-width 104000 \\
            --number-threads 80 \\
            --track-end-frequency 0.6fISCO \\
            --track-max-length 0.45 \\
            --posterior-summary-file summary_statistics.dat \\
            --posterior-param-type maxL  \\
            --output-filename out.h5 --verbose

Using GraceDB G-event ID:
inspiralhom_analysis  --detector "L1" \\
            --frame-type "L1_HOFT_C01" \\
            --channel-name "L1:DCS-CALIB_STRAIN_CLEAN_C01" \\
            --window-width 24000 \\
            --gracedb-id 'G329483' \\
            
Specifying the intrinsic parameters and event times:
inspiralhom_analysis  --detector "L1" \\
            --frame-type "L1_HOFT_C01" \\
            --channel-name "L1:DCS-CALIB_STRAIN_CLEAN_C01" \\
            --window-width 24000 \\
            --mass1 [] --mass2 [] \\
            --spin1z [] --spin2z [] \\
            --event-time [] --det-event-time [] \\
"""

parser = argparse.ArgumentParser(description=__doc__[1:], \
    formatter_class=argparse.ArgumentDefaultsHelpFormatter, usage=usage)

parser.add_argument("--detector",
                type=str, default=None, required=True, action="store",
                help="Sepcify name of the detector. REQUIRED")

parser.add_argument("--frame-type",
                type=str, default=None, required=True, action="store",
                help="Set the type of the gravitational wave frame file. "
                     " REQUIRED")

parser.add_argument("--channel-name",
                type=str, default=None, required=True, action="store",
                help="The channel containing the gravitational strain data. "
                     "REQUIRED")

parser.add_argument("--window-width",
                type=int, required=True, default=None, action="store",
                help="Set the width of the window for extracting the data. "
                     "The analysis fetches the data surrounding the event "
                     "(between 'event_time-window_width/2', and "
                     "'event_time-window_width/2' ) for estimating the "
                     "background. UNIT: Sec, REQUIRED. ")

parser.add_argument("--science-segment-file",
                   type=str, default=None, action="store",
                   help="A string that contains the path to xml files that "
                        "contains a segment table of science data, OPTIONAL. ")

parser.add_argument("--veto-segment-file",
                   type=str, default=None, action="store",
                   help="A string that contains the path to xml file that "
                        "contains a segment veto table, OPTIONAL. ")

parser.add_argument("--chunk-length",
                type=int, default=64, action="store",
                help="Set the chunk length. For on-source data, the analysis "
                     "fetches the data on either side of the event time "
                     "(\pm chunk_length/2), and then compute the PSD to "
                     "whiten the raw data. UNIT: Sec, OPTIONAL.")

parser.add_argument("--approximant",
                type=str, default='SEOBNRv4HM', action="store", required=True,
                help="Specify the approximant to use for waveform generation."
                     "Choose from %s" %(', '.join(utils.waveforms_list) ))

parser.add_argument("--track-end-frequency",
                type=str, default='fISCO', action="store",
                help="Set the ending frequency of the (2, 2) mode "
                     "time-frequency track. The analysis assumes that "
                     "the relation f_lm(t) = m/2 f_22(t) is valid in the "
                     "inspiral phase. If the user wants to set the ending "
                     "frequency at 0.6*fISCO, then use '0.6fISCO'. "
                     "UNIT: fISCO. OPTIONAL")


parser.add_argument("--track-max-length",
                type=float, default=None, action="store", required=True,
                help="Set the maximum length of the track. The length is "
                     "calculated by assuming the track is ending at the time "
                     "of coalescence. Since the analysis is summing the pixels "
                     "energies along the scaled tracks, so the longer track "
                     "can lead to a smaller signal-to-noise ratio. "
                     "UNIT: Sec, REQUIRED")


parser.add_argument("--posterior-summary-file",
                type=str, default=None, action="store",
                help="Provide the summary file of the parameter estimation "
                     "study. OPTIONAL")

parser.add_argument("--posterior-param-type",
                type=str, default='maxL', action="store",
                help="Provide the type of posterior sample. It is required "
                     "to extract the parameters from the posterior summary "
                     "file. OPTIONAL")

parser.add_argument("--gracedb-id",
                type=str, default=None, action="store",
                help="Supply the GraceDB G-event ID . OPTIONAL")

parser.add_argument("--mass1",
                type=float,  default=None, action="store",
                help="Set the mass of the first object. UNITS=Solar mass, "
                     "OPTIONAL. ")

parser.add_argument("--mass2",
                type=float,  default=None, action="store",
                help="Set the mass of the second object. UNITS=Solar mass, "
                     "OPTIONAL. ")

parser.add_argument("--spin1z",
                type=float,  default=None, action="store",
                help="Set the dimensionless spin of the first object: "
                     "OPTIONAL.")

parser.add_argument("--spin2z",
                type=float,  default=None, action="store",
                help="Set the dimensionless spin of the second object: "
                     "OPTIONAL.")

parser.add_argument("--event-time",
                type=float,  default=None, action="store",
                help="GPS time of the event at the geocentric coordinate. "
                     "OPTIONAL.")

parser.add_argument("--det-event-time",
                type=float,  default=None, action="store",
                help="GPS time of the event at the detector coordinate. "
                     "OPTIONAL.")

parser.add_argument("--fmin",
                type=float, default=15.0, action="store",
                help="Set lower cutoff frequency for generating the "
                     "templates. Metric is used to calculate the match. "
                     "UNITS=Hz, OPTIONAL")

parser.add_argument("--deltaT",
                type=float, default=1/4096.0, action="store", 
                help="Set the time sampling rate. UNITS=Sec, OPTIONAL")

parser.add_argument("--deltaF",
                type=float, default=1/5.0, action="store", 
                help="Set the frequency sampling rate. UNITS=Hz, OPTIONAL")

parser.add_argument("--tref",
                type=float, default=4.0, action="store", 
                help="Set the reference time of the event. The analysis "
                     "crops the data by assuming the reference time as a "
                     "coalescence time of the event. UNITS=Sec., OPTIONAL")

parser.add_argument("--min-alpha",
                type=float, default=0.2, action="store", 
                help="Set the minimim of the scale parameter of the "
                     "22-track. UNITS=Dimensionless, OPTIONAL.")

parser.add_argument("--max-alpha",
                type=float, default=2.9, action="store", 
                help="Set the maximum of the scale parameter of the "
                     "22-track. UNITS=Dimensionless. OPTIONAL.")


parser.add_argument("--delta-alpha",
                type=float, default=0.005, action="store", 
                help="Set the step in scale between two consecutive "
                     "scaled tracks. UNITS=Dimensionless. OPTIONAL.")


parser.add_argument("--cwt-fhigh",
                type=float, default=None, action="store", 
                help="Set the maximum frequency in the computation of "
                     "CWT. It must be higher than the ending frequency "
                     "of maximally scaled track i.e. "
                     "cwt_fhigh>max_alpha*ending frequency of the 22-track. "
                     "UNITS=Hz, OPTIONAL. ")

parser.add_argument("--ndm-freq",
                type=float, default=None, action="store", 
                help="Set the nondiemnsional frequency of the Morlet wavelt "
                     "for CWT UNITS=Dimensionless, OPTIONAL. ")
 

parser.add_argument("--number-threads",
                type=int, default=1, action="store",
                help="Set the number of threads for parallel processing "
                     "It is highly required for faster background "
                     "estimation. OPTIONAL")

parser.add_argument("--output-filename",
                type=str, required=True, default="output.h5",
                help="Output filename of the analysis. REQUIRED.")

parser.add_argument("--verbose", action="store_true", default=False, 
                help="verbose output")


args = parser.parse_args()
if args.verbose:
    log_level = logging.INFO
else:
    log_level = logging.WARN
logging.basicConfig(format='%(asctime)s %(message)s', level=log_level)


logging.info("Set the instance attributes of utils object.")
mUtils.approx = args.approximant
mUtils.detector = args.detector
mUtils.fmin = args.fmin
mUtils.deltaT = args.deltaT
mUtils.fmax = 0.5/args.deltaT
mUtils.deltaF = args.deltaF

mUtils.tref = args.tref
mUtils.frame_type = args.frame_type
mUtils.channel_name = args.channel_name
mUtils.chunk_length = args.chunk_length

# Set a numpy array of scale paramters
mUtils.alpha_arr = np.linspace( args.min_alpha, args.max_alpha, \
           int( (args.max_alpha -args.min_alpha)/args.delta_alpha + 1 ) )


# Set the intrinsic and extrinsic parameters
if args.posterior_summary_file is not None:
    mUtils._set_params_from_summary_of_posterior_samples(args.posterior_summary_file, \
                                        _type=args.posterior_param_type)
elif args.gracedb_id is not None:
    mUtils._set_gracedb_params(args.gracedb_id)
else:
    mUtils.mass1 = args.mass1
    mUtils.mass2 = args.mass2
    mUtils.spin1z = args.spin1z
    mUtils.spin2z = args.spin2z
    mUtils.event_time = args.event_time
    mUtils.det_event_time = args.det_event_time


    
# Set the parameters for 22-track, f_22(t) and CWT parameters
fSchwarzISCO = 1.0 / ( 6.0**1.5 * lal.PI * (mUtils.mass1 + mUtils.mass2) * lal.MTSUN_SI )
if args.track_end_frequency=='fISCO':
    mUtils.track_end_frequency = fSchwarzISCO
else:
    mUtils.track_end_frequency = float(args.track_end_frequency[ \
                   :args.track_end_frequency.index('fISCO')]) * fSchwarzISCO
mUtils.track_max_length = args.track_max_length

if args.cwt_fhigh == None:
    mUtils.cwt_fhigh = 10.0*(mUtils.track_end_frequency*mUtils.alpha_arr[-1]//10 + 2)
else:
    mUtils.cwt_fhigh = args.cwt_fhigh


# The analysis uses strain data with a length of chunk_length on either side
# of the event time (\pm chunk_length/2). Whiten the data using it's PSD.
# |<-- chunk_length/2 -->|<-- chunk_length/2 -->|
# |<---------------------|--------------------->|
# |<------------- chunk_length ---------------->|

# Crop a small sub-chunk surrounding the event time, and then analyses the data.
# The frame of the analysis on the sub-chunk:
# <-----  sub-chunk ----------->
# |------------------|----------|
# t_start            t_ref      t_end
# t_ref: event_time (time of coalescence)
# Fetch the on-source data surrounding event time (\pm chunk_length/2)
logging.info("Load the data surrounding the event time.")

half_length = mUtils.chunk_length//2
strain = pycbc.frame.query_and_read_frame(args.frame_type, args.channel_name, \
    int(mUtils.det_event_time - half_length), int(mUtils.det_event_time + half_length))
# Resample the data 
if strain.delta_t != mUtils.deltaT:
    strain = resample_to_delta_t(strain, mUtils.deltaT)

# Calculate the noise spectrum Welch median estimation
# PSD for each chunk
PSDchunk = pycbc.psd.interpolate(pycbc.psd.welch(strain), 1.0 / strain.duration)
# PSD for each small slice
psd = pycbc.psd.interpolate(pycbc.psd.welch(strain), mUtils.deltaF)


# Whiten the raw strain
wtseries = mUtils._whiten(strain, PSDchunk)


# Crop the strain so that croped-strain has length of duration and event time at tref
start = mUtils.det_event_time - int(mUtils.det_event_time) + half_length - mUtils.tref
sidx = int(  start/mUtils.deltaT)
eidx = int( (start + 1/mUtils.deltaF)/mUtils.deltaT)
wstrain = pycbc.types.TimeSeries(wtseries.data[sidx:eidx], delta_t=mUtils.deltaT)


logging.info("Calculate the nondimensional frequency of the Morlet wavelet.")
# Generate the time-frequency track of the (2, 2) mode
tArrFixDT, fArrFixDT, interpF, interpT = \
           mUtils._gen_time_frequency_path(start_freq=mUtils.fmin)


# Calculate the nondimensional frequency of the Morlet wavelet for CWT
if args.ndm_freq ==None:
    mUtils.ndm_freq = FindNonDimensionalFrequency(mUtils, wstrain.data)
else:
    if args.ndm_freq < 5:
        raise ValueError('Wavelet central frequency is less than 5. Cg approximation is not valid')
    else:
        mUtils.ndm_freq = args.ndm_freq
sys.stdout.write("Nondimensional frequency in Morlet wavelet: %s \n"%mUtils.ndm_freq)

# Caclculate admissibility constant (C_g) of Morlet wavelet
# C_g = integrate_0^infinity |\psi(f)|^2 /f  df, where \psi(f) is the Morlet wavelet
# in frequency domain. The peak of the integrand occurs at the value of w_0. Therefore,
# choosing upper limit of integration to be 2*w_0 is sufficient to capture most of the integral.
# where w_0 = nondimensional frequency. For this choice, the maximum error in C_g can appear for the case
# small value of w_0. In our analysis, w_0>5. For w_0 = 5, the error in C_g is < 10^-11 whereas 
# the C_g nearly equal to 1/w_0.

# Morlet wavelet in Frequency domain
# \psi(w) = 2^{1/2} \pi^{1/4} [ \exp(-(w - w0)^2/2) - \exp( -(w0^2 - w^2)/2 )  ]
# Here w = 2 \pi f
# The second term in the bracket is known as the correction term to preserve the zero mean of
# the first term.
# The admissibility condition of a wavelet is to guarantee that the integral is finite,
# we must ensure that \psi(0) = 0, which explains why wavelets must have a zero average.
# However, the pycwt.Morlet does not include that small additional term, \psi(0) != 0, so the integral
# is actually divergent. But this does not affect the numerical evaluation since the additional term
# becomes negligible for values of w_0>>1.

mother = pycwt.Morlet(mUtils.ndm_freq)
mUtils.wavelet_Cg = sp.integrate.quad( lambda x: np.abs( mother.psi_ft(x))**2/x, 0.0, 2.0*mUtils.ndm_freq )[0]

wave, scales, freqs = mUtils._compute_cwt(wstrain.data, norm=True)[:3]
power = np.abs(wave)**2.0


# Collect the energies along the scaled tracks tracks
Yalpha = mUtils._stack_pixels_energies(power, tArrFixDT, fArrFixDT, interpT)



# Restrict the lower cutoff frequency of the timedomain waveform 
# for generating the template model if the approximant is not a 
# precessing spin SEOB waveform.
fmin_old = mUtils.fmin
if not mUtils.approx.startswith('SEOBNRv3') or \
    not mUtils.approx.startswith('SEOBNRv4P'):
    try:
        # Set a lower frequency for generating the time-domain waveform that are used to
        # construct the template S(\alpha). This choice makes sure that the length of
        # time-domain waveform is less than the value of tref. 
        # The default configuration assumes that deltaF=1/5 and tref=4.0, then the length of the
        # time-domain waveform is 3.8. If we generate a waveform with a length of >= tref (4 sec),
        # then the rolled waveform can have a tail at the right side after zero padding and rolling
        # instead of the left side. The choice of small value 0.2 makes sure that the rolling will
        # always occur towards the right. One can choose any other value (DT) instead of 0.2 but that
        # DT should be less than tref and also satisfy tref-DT > track length.
        # The interpolated functions (interpT, interpF) assume time is going backward with a colaescence
        # time t=0.
        start_freq = max(interpF(mUtils.tref-0.2), mUtils.fmin)
        mUtils.fmin = float(start_freq)
    except: pass
else: pass

# Generate the template model by considering the specific modes
mUtils.mode_list = [(2, 2)]
Salpha22 = mUtils._gen_template_model( psd, tArrFixDT, fArrFixDT, interpT)[0]

mUtils.mode_list = [ (3, 3)]
Salpha33 = mUtils._gen_template_model(psd, tArrFixDT, fArrFixDT, interpT)[0]

mUtils.mode_list = [ (4, 4)]
Salpha44 = mUtils._gen_template_model( psd, tArrFixDT, fArrFixDT, interpT)[0]




# back ground estimation using the off-source data
logging.info("Estimate the background")
nbg = noisebg.GenerateNoiseBG(mUtils)

nalpha_dic = nbg.main(tArrFixDT, fArrFixDT, interpT, \
                int(mUtils.event_time-args.window_width//2), int(mUtils.event_time+args.window_width//2), \
                nprocess=args.number_threads, science_segment_file=args.science_segment_file, \
                veto_segment_file=args.veto_segment_file)


nKeys = sorted( nalpha_dic.keys() )
nslices = len(nalpha_dic[ nKeys[0] ][0] )
Nalpha_data_full = nalpha_dic[ nKeys[0] ][0]
Times = np.array([ nKeys[0] ]*nslices)
for key in nKeys[1:]:
    Nalpha_data_full = np.vstack(( Nalpha_data_full, nalpha_dic[ key ][0] ))
    Times = np.hstack(( Times, np.array([key]*nslices) ))
    


# TO-DO: Delete the Nalpha series whose first few value is very high
# TF-map may have hot spots in the low frequency region numrical error,
# which is mostly visible in C00 data.
#idxx = np.unique(np.hstack(( np.where( np.max(Nalpha_data_full, axis=1) >100000 )[0], \
#                                np.where( Nalpha_data_full[:,0] >2500 )[0] )))
#Nalpha_dataFull = np.delete(Nalpha_data_full, idxx, axis=0)

Nalpha_dataFull = Nalpha_data_full.copy()

# Compute the covariance matrix using 2400 Nalpha samples, and
# remaining samples are used to compute p(\beta | H_0)
midIdx = np.where(Times > mUtils.event_time )[0][0]
deltaIdx = 1200
Nalpha_data = Nalpha_dataFull.copy()[midIdx-deltaIdx:midIdx+deltaIdx]

# Mean of N(\alpha)
mualpha = np.mean(Nalpha_data, axis=0)

# Compute the covariance matrix of N(\alpha)
# \Sigma(\alpha, \alpha') = E[ (N(alpha) -\mu(a\lpha)) (N(\alpha')-\mu(\alpha'))^T ]
xy = np.vstack([Nalpha_data[:,j] - mualpha[j] for j in range(len(mualpha))])
npcov = np.cov(xy)

# Inverse of covariance matrix
covinv = np.linalg.pinv(npcov)


# Maximize the LLR for unknown overall amplitudes for each 'm' mode
# Noise;   H_0: Y(\alpha) = N(\alpha)
# m=2;     H_1: Y(\alpha) = N(\alpha) + a_2 S_2(\alpha); one unknown amplitude
# m=2,3;   H_1: Y(\alpha) = N(\alpha) + a_2 S_2(\alpha) + a_3 S_3(\alpha); two unknown amplitude
# m=2,3,4; H_1: Y(\alpha) = N(\alpha) + a_2 S_2(\alpha) + a_3 S_3(\alpha) + a_3 S_3(\alpha); three unknown amplitude

# Maximize the LLR for signal model m=2 mode.
initial_guess = [1.0]
res22 = opt.minimize(utils.NegativeLogLikelihoodRatio, initial_guess, \
        args=[ [Salpha22], Yalpha, covinv, mualpha], method='Powell', \
        options={'ftol': 1e-20} )

# Maximize the LLR for m=2 and m=3 modes.
initial_guess = [1.0, 1.0]
res22_33 = opt.minimize(utils.NegativeLogLikelihoodRatio, initial_guess, \
           args=[ [Salpha22, Salpha33], Yalpha, covinv, mualpha], \
           method='Powell', options={'ftol': 1e-20} )

# Maximize the LLR for m=2, m=3 and m=4 modes.
initial_guess = [1.0, 1.0, 1.0]
res22_33_44 = opt.minimize(utils.NegativeLogLikelihoodRatio, initial_guess, \
              args=[ [Salpha22, Salpha33, Salpha44], Yalpha, covinv, mualpha], \
              method='Powell', options={'ftol': 1e-20} )

# Compute the final statistic \beta
sArr33 = res22_33.x[1]*Salpha33
gammasq33 = np.dot( sArr33, np.dot(covinv, sArr33) )
beta33 = np.dot(Yalpha-res22_33.x[0]*Salpha22-mualpha, np.dot(covinv, sArr33))/gammasq33**0.5

sArr44 = res22_33_44.x[2]*Salpha44
gammasq44 = np.dot( sArr44, np.dot(covinv, sArr44) )
beta44 = np.dot(Yalpha-res22_33_44.x[0]*Salpha22 - res22_33_44.x[1]*Salpha33 - \
               mualpha, np.dot(covinv, sArr44))/gammasq44**0.5



# Compute the p-value for quantifying the evidence of higher-order modes
# Calculate the distribution p(\beta | H_0) using the off-source data.
# Formula of \beta for off-source data: \beta = < (Y-\mu) | S >/gamma
betaList33, betaList44 = [], []
SalphaList = [[], []]
for ii in range(deltaIdx+nslices, Nalpha_dataFull.shape[0]-deltaIdx-nslices):
    # Nalpha samples surrounding the ii-th Nalpha sample, avoid adjacent slices
    Nalpha_data = np.vstack(( Nalpha_dataFull[ii-deltaIdx-nslices: ii-nslices], \
                             Nalpha_dataFull[ii+nslices:  ii+deltaIdx+nslices] ))
    # Mean of N(\alpha)
    mualpha_ii = np.mean(Nalpha_data, axis=0)
    # Compute the covariance matrix of Nalpha
    xy = np.vstack([Nalpha_data[:,j] - mualpha_ii[j] for j in range(len(mualpha_ii))])
    npcov = np.cov(xy)
    covinv = np.linalg.pinv( npcov )
    
    # Use the template which was constructed usning a psd
    # where the psd was estimated using the chunk corresponds to ii-th Nalpha 
    sArr33 = nalpha_dic[ Times[ii] ][1]
    sArr44 = nalpha_dic[ Times[ii] ][2]
    SalphaList[0].append(sArr33)
    SalphaList[1].append(sArr44)
    
    gammasq33 = np.dot( sArr33, np.dot(covinv, sArr33) )
    gammasq44 = np.dot( sArr44, np.dot(covinv, sArr44) )
    nArr = Nalpha_dataFull[ii] - mualpha_ii
    b33 = np.dot( nArr, np.dot(covinv, sArr33) )/gammasq33**0.5
    b44 = np.dot( nArr, np.dot(covinv, sArr44) )/gammasq44**0.5
    betaList33.append( b33 )
    betaList44.append( b44 )

betaList33 = np.array(betaList33)
betaList44 = np.array(betaList44)


sys.stdout.write("Results for m=3 mode: \n")
sys.stdout.write("LLR(22)= %1.2f \nLLR(22+33)= %1.2f, beta(33)= %1.2f \n" % \
                 (-res22.fun, -res22_33.fun, beta33))

nSamples1 = len(betaList33)
nSamples2 = len( np.where( betaList33 > beta33 )[0])
sys.stdout.write("Total number of off-source samples = %s \n" % nSamples1 )
sys.stdout.write("Number of off-source samples larger than on-source = %s \n" % nSamples2 )

if nSamples2 > 0:
    sys.stdout.write("p-value = %d/%d \n" % (nSamples2, nSamples1))
    pval_3 = nSamples2/nSamples1
else:
    sys.stdout.write("p-value < %d/%d \n" % (1.0, nSamples1))
    pval_3 = 1.0/nSamples1
    
sys.stdout.write("Highest value off-source beta = %s \n" % max(betaList33) )

# Fit the emperical distribution of p(\beta | H_0) with a Gaussian distribution
loc33, scale33 = sts.norm.fit(betaList33)
loc44, scale44 = sts.norm.fit(betaList44)

# Calulate p-value by integrating p(\beta | H_0) from event \beta to infinity.
pvalue33 = 0.5*spl.erfc( (beta33-loc33)/scale33/2**0.5 )
pvalue44 = 0.5*spl.erfc( (beta44-loc44)/scale44/2**0.5 )

sys.stdout.write("Gaussian assumption of background: p-value = %g \n \n" % pvalue33 )

sys.stdout.write("Results for m=4 mode: \n")
sys.stdout.write("LLR(22+33+44)=%1.2f, beta(44)= %1.2f \n"%(-res22_33_44.fun, beta44))
nSamples1 = len(betaList44)
nSamples2 = len( np.where( betaList44 > beta44 )[0])
sys.stdout.write("Total number of off-source samples = %s \n" % nSamples1 )
sys.stdout.write("Number of off-source samples larger than on-source = %s \n" % nSamples2 )
if nSamples2 > 0:
    sys.stdout.write("p-value = %d/%d \n" % (nSamples2, nSamples1))
    pval_4 = nSamples2/nSamples1
else:
    sys.stdout.write("p-value < %d/%d \n" % (1, nSamples1))
    pval_4 = 1.0/nSamples1
    
sys.stdout.write("Number of off-source samples larger than on-source = %s \n" % nSamples2 )
sys.stdout.write("Highest value off-source beta = %s \n" % max(betaList44) )

sys.stdout.write("Gaussian assumption of background: p-value = %g \n \n" % pvalue44 )



# Write the outputs from the analysis
logging.info("Writing the output from analysis")
hf = h5py.File( args.output_filename, 'w')

# Write the input parameters
ip = hf.create_group('inputparams')
if args.posterior_summary_file is not None:
    ip.create_dataset('posfile', data=args.posterior_summary_file)
    ip.create_dataset('paramstype', data=args.posterior_param_type)
elif args.gracedb_id is not None:
    ip.create_dataset('GraceDBid', data=args.gracedb_id)
   

ip.create_dataset('deltaT',  data=mUtils.deltaT )
ip.create_dataset('deltaF',  data=mUtils.deltaF )
ip.create_dataset('tref',  data=mUtils.tref )
ip.create_dataset('approximant',  data=mUtils.approx )
ip.create_dataset('mass1',  data=mUtils.mass1)
ip.create_dataset('mass2',  data=mUtils.mass2)
ip.create_dataset('spin1z', data=mUtils.spin1z)
ip.create_dataset('spin2z', data=mUtils.spin2z)
ip.create_dataset('iota',   data=mUtils.iota)
ip.create_dataset('dist',   data=mUtils.dist)
ip.create_dataset('dec',    data=mUtils.theta)
ip.create_dataset('ra',     data=mUtils.phi)
ip.create_dataset('psi',    data=mUtils.psi)
ip.create_dataset('phiref', data=mUtils.phiref)
ip.create_dataset('ifo',    data=mUtils.detector)
ip.create_dataset('t0',     data=mUtils.event_time)
ip.create_dataset(mUtils.detector.lower() + "_end_time", data=mUtils.det_event_time)

ip.create_dataset( "track_end_freq", data=args.track_end_frequency)
ip.create_dataset( "track_max_length", data=args.track_max_length)

ip.create_dataset('frame_type',  data=mUtils.frame_type)
ip.create_dataset('channel_name', data=mUtils.channel_name)
ip.create_dataset('chunk_length', data=mUtils.chunk_length)

# Write the N(\alpha) samples
nbg = hf.create_group('noisebg')
Nalpha_data_full = np.hstack(( Times[:,None], Nalpha_data_full ))
nbg.create_dataset( 'nalphas', data=Nalpha_data_full)
nbg.create_dataset( 'SalphaList22', data=np.array(SalphaList[0]) )
nbg.create_dataset( 'SalphaList33', data=np.array(SalphaList[1]) )
nbg.create_dataset( 'Times', data=nalpha_dic.keys())
    
# Write the CWT params and time-frequency-map of the event
cwt = hf.create_group('CWT')
cwt.create_dataset('ndmfreq', data=mUtils.ndm_freq)
cwt.create_dataset('Cg', data=mUtils.wavelet_Cg)
cwt.create_dataset('scales', data=scales)
cwt.create_dataset('freqs', data=freqs)
cwt.create_dataset('times', data=np.linspace( mUtils.det_event_time - \
       mUtils.tref, mUtils.det_event_time+1, int(1.0/mUtils.deltaT/mUtils.deltaF)))
cwt.create_dataset('cwt', data=wave)
cwt.create_dataset('tArrFixDT', data=tArrFixDT)
cwt.create_dataset('fArrFixDT', data=fArrFixDT)


# Write the analysis output
stc = hf.create_group('analysis')
stc.create_dataset('psd',       data=psd.data)
stc.create_dataset('freqs',     data=psd.sample_frequencies)
stc.create_dataset('alpha_arr', data=mUtils.alpha_arr)
stc.create_dataset('Yalpha',    data=Yalpha)
stc.create_dataset('mualpha',   data=mualpha)
stc.create_dataset('Salpha22',  data=Salpha22)
stc.create_dataset('Salpha33',  data=Salpha33)
stc.create_dataset('Salpha44',  data=Salpha44)
stc.create_dataset('LLR22',     data=-res22.fun)
stc.create_dataset('LLR22_33',    data=float(-res22_33.fun))
stc.create_dataset('LLR22_33_44', data=float(-res22_33_44.fun))
stc.create_dataset('beta33',   data=float(beta33))
stc.create_dataset('beta44',   data=float(beta44))
stc.create_dataset('pvalue33',   data=float(pval_3))
stc.create_dataset('pvalue44',   data=float(pval_4))
stc.create_dataset('gaussian_pvalue33', data=float(pvalue33))
stc.create_dataset('gaussian_pvalue44', data=float(pvalue44))
stc.create_dataset('beta33H0', data=betaList33)
stc.create_dataset('beta44H0', data=betaList44)

hf.close()

