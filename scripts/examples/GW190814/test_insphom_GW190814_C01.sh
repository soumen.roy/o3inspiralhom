## Link of the summary statistics page
## https://galahad.aei.mpg.de/~rcotesta/LVC/offline_pe/S190814bv/SEOBNRv4HM_ROM_minus/
python inspiralhom_analysis.py --detector L1 \
            --frame-type L1_HOFT_C01 --channel-name L1:DCS-CALIB_STRAIN_CLEAN_C01 \
            --window-width 34000 --number-threads 70 \
            --approximant SEOBNRv4HM \
            --track-end-frequency 0.6fISCO \
            --track-max-length 0.45 \
            --posterior-summary-file summary_statistics.dat \
            --posterior-param-type maxL  \
            --output-filename GW190814bv_L1_C01_fISCO_0p45.h5
