## Link of the summary statistics page
## https://galahad.aei.mpg.de/~rcotesta/LVC/offline_pe/S101412m/C01/A1-C01-SEOBNRv4HM_ROM_60Hz_non_linear_nest/SEOBNRv4HM_ROMpseudoFourPN/1239082262.22-329473/V1H1L1/summary_statistics.dat

## Link of posterior samples
## https://galahad.aei.mpg.de/~rcotesta/LVC/offline_pe/S190412m/final_results_review/SEOBNRv4HM_ROM_nest/samples/posterior_samples.json
# The instrinsic paramaters are correspond to maxL sample

python inspiralhom_analysis.py --detector L1 \
            --frame-type L1_HOFT_C01 --channel-name L1:DCS-CALIB_STRAIN_CLEAN_C01 \
            --window-width 34000 --number-threads 70 \
            --approximant SEOBNRv4HM \
            --track-end-frequency 0.6fISCO \
            --track-max-length 0.45 \
            --mass1 33.76798828818286 --mass2 9.700004180482646 \
	    --spin1z 0.28558620162670567 --spin2z 0.03826741661275385 \
	    --event-time 1239082262.181084 \
	    --det-event-time 1239082262.1614943 \
            --output-filename S190412m_L1_C01_0p6fISCO_0p45.h5
