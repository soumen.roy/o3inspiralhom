"""
Create the plots from the ouptut file of inspiralhom_analysis.
"""

from __future__ import division
import sys
import numpy as np
import h5py

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1.axes_divider import make_axes_locatable
from mpl_toolkits.axes_grid1.colorbar import colorbar

import matplotlib.ticker as ticker
  

import pycbc.frame
import pycbc
from pycbc.filter import resample_to_delta_t
    
import utils, lal
mUtils = utils.MiscellaneousUtils()
    
import argparse
from ConfigParser import ConfigParser

__author__ = "Soumen Roy <soumen.roy@ligo.org>"
__program__ = "read_insphom_results"
__version__ = "no version"

usage = """ 

python read_results.py --input-file gw190814_L1_C01_4096_0p6fISCO_0p45_strip.h5 \
                        --rank-analysis-file gw190814_rank_analysis.h5 \
                        --event-name gw190814 \
                        --tfmap-tmin -3.0 \
                        --tfmap-fmax 200.0 \
                        --yalpha-subax-ymin 0.15 \
                        --yalpha-subax-ymax 2.5 \
                        --yalpha-ymax 9.0
                        
python read_results.py --input-file gw190412_L1_C01_60Hz_4096_0p6fISCO_0p45_strip.h5 \
                        --rank-analysis-file gw190412_rank_analysis.h5 \
                        --event-name gw190412 \
                        --tfmap-tmin -1.0 \
                        --tfmap-fmax 200.0 \
                        --yalpha-subax-ymin 0.15 \
                        --yalpha-subax-ymax 0.85 \
                        --yalpha-ymax 7.0
                        
"""

parser = argparse.ArgumentParser(description=__doc__[1:], \
    formatter_class=argparse.ArgumentDefaultsHelpFormatter, usage=usage)

parser.add_argument("--input-file",
                type=str, default=None, required=True, action="store",
                help="Supply the name of the input hdf5 file. REQUIRED")

parser.add_argument("--rank-analysis-file",
                type=str, default=None, action="store",
                help="Supply the name of the input hdf5 file produced "
                     " from covariance matrix rank analysis. OPTIONAL")

parser.add_argument("--matplotlib-params-style-file",
                type=str, default=None,  action="store",
                help="Supply the name of matplotlib parameters style "
                     " file (.sty). OPTIONAL")

parser.add_argument("--event-name",
                type=str, default='GW0000', action="store",
                help="Supply the name of Event. OPTIONAL")

parser.add_argument("--output-file",
                type=str, default=None, action="store",
                help="Supply the name of the input hdf5 file. OPTIONAL")

parser.add_argument("--tfmap-tmin",
                type=float, default=-1.0, action="store",
                help="Set the minimum value of time axis. OPTIONAL")

parser.add_argument("--tfmap-tmax",
                type=float, default=0.2, action="store",
                help="Set the maximum value of time axis. OPTIONAL.")

parser.add_argument("--tfmap-fmin",
                type=float, default=1.0, action="store",
                help="Set the minimum value of frequency axis. OPTIONAL")

parser.add_argument("--tfmap-fmax",
                type=float, default=200.0, action="store",
                help="Set the maximum value of frequency axis. OPTIONAL.")

parser.add_argument("--yalpha-subax-ymin",
                type=float, default=0.15, action="store",
                help="Set the minimum value of y-axis in subaxes plot of Yalpha. OPTIONAL")

parser.add_argument("--yalpha-subax-ymax",
                type=float, default=2.0, action="store",
                help="Set the maximum value of y-axis in subaxes plot of Yalpha. OPTIONAL.")


parser.add_argument("--yalpha-ymin",
                type=float, default=0.0, action="store",
                help="Set the minimum value of y-axis in Yalpha plot. OPTIONAL")

parser.add_argument("--yalpha-ymax",
                type=float, default=9.0, action="store",
                help="Set the maximum value of y-axis in Yalpha plot. OPTIONAL.")



args = parser.parse_args()

matplotlib.rcParams['text.latex.preamble'] = [r'\usepackage{amsmath}',r'\mathchardef\mhyphen="2D']
if args.matplotlib_params_style_file is not None:
    plt.style.use(args.matplotlib_params_style_file)
else:
    matplotlib.rcParams['figure.figsize'] =  6.75, 5
    matplotlib.rcParams['axes.labelsize'] = 18
    matplotlib.rcParams['legend.fontsize'] = 18
    matplotlib.rcParams['xtick.labelsize'] = 18
    matplotlib.rcParams['ytick.labelsize'] = 18
    matplotlib.rcParams['font.size'] = 18
    matplotlib.rcParams['savefig.dpi'] = 300
    matplotlib.rcParams['font.family'] = 'Times New Roman'
    matplotlib.rcParams['font.sans-serif'] = ['Bitstream Vera Sans']
    matplotlib.rcParams['text.usetex'] = True
    matplotlib.rcParams['mathtext.fontset'] = 'stixsans'


# check the file format
if args.input_file.endswith('h5'):
    pass
elif args.input_file.endswith('hdf5'):
    pass
else:
    raise ValueError('Input file must be hdf5 format')

# Load the input file
f = h5py.File(args.input_file, 'r')
if f.keys()[0] == 'stripdata':
    f = f['stripdata']
    stripdata = True
else:
    stripdata = False

    
# Load rank analysis file
if args.rank_analysis_file is not None:
    rf = h5py.File(args.rank_analysis_file, 'r')
    beta33 = rf['fix_rank_analysis']['beta33'][()]
    beta33H0 = rf['fix_rank_analysis']['beta33H0'][()]
else:
    if stripdata == False:
        beta33 = f['analysis']['beta33'][()]
        beta33H0 = f['analysis']['beta33H0'][()]
    else:
        beta33 = f['fix_rank_analysis']['beta33'][()]
        beta33H0 = f['fix_rank_analysis']['beta33H0'][()]


sys.stdout.write("beta33 = %s \n" % beta33 )
sys.stdout.write("Total number of off-source samples = %s \n" % len(beta33H0) )
sys.stdout.write("Number of off-source samples larger than on-source = %s \n" % \
                 len( np.where( beta33H0 > beta33 )[0]) )
sys.stdout.write("Highest value of off-source beta = %s \n" % max( beta33H0 ))

nSamples1 = len(beta33H0)
nSamples2 = len( np.where( beta33H0 > beta33 )[0])

if nSamples2 > 0:
    sys.stdout.write("p-value = %d/%d \n \n" % (nSamples2, nSamples1))
    pval_3 = nSamples2/nSamples1
else:
    sys.stdout.write("p-value < %d/%d \n \n" % (1.0, nSamples1))
    pval_3 = 1.0/nSamples1

mUtils.mass1 = f['inputparams']['mass1'][()]
mUtils.mass2 = f['inputparams']['mass2'][()]
mUtils.spin1z = f['inputparams']['spin1z'][()]
mUtils.spin2z = f['inputparams']['spin2z'][()]


mUtils.detector = f['inputparams']['ifo'][()]
mUtils.event_time = f['inputparams']['t0'][()]
mUtils.det_event_time = f['inputparams']['%s_end_time'%mUtils.detector.lower() ][()]
mUtils.channel_name = f['inputparams']['channel_name'][()]
mUtils.frame_type = f['inputparams']['frame_type'][()]
mUtils.chunk_length = f['inputparams']['chunk_length'][()]

mUtils.approx = f['inputparams']['approximant'][()]
mUtils.deltaF = f['inputparams']['deltaF'][()]
mUtils.fmax = 0.5/f['inputparams']['deltaT'][()]
mUtils.deltaT = f['inputparams']['deltaT'][()]
mUtils.tref = f['inputparams']['tref'][()]
mUtils.fmin = 15.0
fISCO = 1.0 / ( 6.0**1.5 * lal.PI * (mUtils.mass1 + mUtils.mass2) * lal.MTSUN_SI )

# Generate the time-frequency track of the (2, 2) mode
tArrFixDT, fArrFixDT, interpF, interpT  = mUtils._gen_time_frequency_path( start_freq=15.0, \
                                       track_max_length=-args.tfmap_tmin, track_end_frequency=1.6*fISCO)
    
    
half_length = mUtils.chunk_length//2
strain = pycbc.frame.query_and_read_frame(mUtils.frame_type, mUtils.channel_name, \
        int(mUtils.det_event_time - half_length), int(mUtils.det_event_time + half_length))

# Resample the data 
if strain.delta_t != mUtils.deltaT:
    strain = resample_to_delta_t(strain, mUtils.deltaT)

# Calculate the noise spectrum Welch median estimation
# PSD for each chunk
PSDchunk = pycbc.psd.interpolate(pycbc.psd.welch(strain), 1.0 / strain.duration)

# Whiten the raw strain
wtseries = mUtils._whiten(strain, PSDchunk)


# Crop the strain so that croped-strain has length of duration and event time at tref
start = mUtils.det_event_time - int(mUtils.det_event_time) + half_length - mUtils.tref
sidx = int(  start/mUtils.deltaT)
eidx = int( (start + 1/mUtils.deltaF)/mUtils.deltaT)
wstrain = pycbc.types.TimeSeries(wtseries.data[sidx:eidx], delta_t=mUtils.deltaT)

mUtils.ndm_freq = f['CWT']['ndmfreq'][()]
mUtils.cwt_fhigh = int(args.tfmap_fmax) + 10
wave, scales, freqs = mUtils._compute_cwt(wstrain.data)[:3]
power = np.abs(wave)**2.0




#############################   
## Plot time-frequency map
##
fig, ax = plt.subplots()

im = ax.imshow(  power , extent=[-4.0, 1.0, \
        min(freqs), max(freqs)],\
           aspect='auto', origin='lower', vmax=round(np.max(power)) )
ax.plot(tArrFixDT-4.0, 1.5*fArrFixDT, lw=0.5, dashes=(5, 10), color='w', label=r'$m=3$ track')

ax.set_ylim(args.tfmap_fmin, args.tfmap_fmax)
ax.set_ylabel(r"Frequency (Hz)")
ax.set_xlabel(r"Time (seconds)")
ax.set_xlim(args.tfmap_tmin, args.tfmap_tmax)

ax.minorticks_on()
ax.tick_params(axis='both',which='minor',length=2,width=1, direction='out')
ax.tick_params(axis='both',which='major',length=5,width=1.5, direction='out')

ax2_divider = make_axes_locatable(ax)
# add an axes above the main axes.
cax2 = ax2_divider.append_axes("top", size="5%", pad="2%", label='Energy density')
cb2 = colorbar(im, cax=cax2, orientation="horizontal", \
               ticks = np.round( np.linspace(0, np.max(power), 7, endpoint=True)) )

cax2.xaxis.set_ticks_position("top")
cb2.set_label_text(r'Energy density')
cax2.xaxis.set_label_position('top')
l = ax.legend(loc=2, facecolor='green')
for text in l.get_texts():
    text.set_color("white")
    
#fig.tight_layout()
figname = 'tfmap_' + str(args.event_name) + '_1.png'
plt.savefig(figname, bbox_inches='tight')



fig, ax = plt.subplots(constrained_layout=True)
im = ax.imshow(  power , extent=[-4.0, 1.0, \
        min(freqs), max(freqs)],\
           aspect='auto', origin='lower', vmax=round(np.max(power)) )

ax.set_ylim(args.tfmap_fmin, args.tfmap_fmax)
ax.set_ylabel(r"\rm{Frequency (Hz)")
ax.set_xlabel(r"\rm{Time  (seconds)}")
ax.set_xlim(args.tfmap_tmin, args.tfmap_tmax)

ax.minorticks_on()
ax.tick_params(axis='both',which='minor',length=2,width=1, direction='out')
ax.tick_params(axis='both',which='major',length=5,width=1.5, direction='out')

ax2_divider = make_axes_locatable(ax)
# add an axes above the main axes.
cax2 = ax2_divider.append_axes("top", size="5%", pad="2%", label='Energy density')
cb2 = colorbar(im, cax=cax2, orientation="horizontal", \
               ticks = np.round( np.linspace(0, np.max(power), 7, endpoint=True)) )
cax2.xaxis.set_ticks_position("top")

cb2.set_label_text(r'\rm{Energy density}')
cax2.xaxis.set_label_position('top')

#fig.tight_layout()

figname = 'tfmap_' + str(args.event_name) + '_2.png'
plt.savefig(figname, bbox_inches='tight')






###############################################################
## Generate the Yalpha plot
##
alpha_arr = f['analysis']['alpha_arr'][:]
Salpha22 = f['analysis']['Salpha22'][:]
Salpha33 = f['analysis']['Salpha33'][:]
Salpha44 = f['analysis']['Salpha44'][:]
Yalpha = f['analysis']['Yalpha'][:]
mualpha = f['analysis']['mualpha'][:]
    
if stripdata == False:

    keys = f['noisebg'].keys()
    Nalpha_dataFulll = f['noisebg']['nalphas'][:][:,1:,]

    Nalpha_dataFull = Nalpha_dataFulll.copy()

    times = np.delete(f['noisebg']['nalphas'][:][:,0], [], axis=0) 

    midIdx = np.where(times > f['inputparams']['l1_end_time'][()] )[0][0]
    
    # Compute the covariance matrix using 2400 Nalpha samples, and
    # remaining samples are used to compute p(\beta | H_0)
    deltaIdx = 1200
    Nalpha_data = Nalpha_dataFull.copy()[midIdx-deltaIdx:midIdx+deltaIdx]

    # Mean at each \alpha
    mualpha = np.mean(Nalpha_dataFull, axis=0)
    xy = np.vstack([Nalpha_data[:,j] - mualpha[j] for j in range(len(mualpha))])

    # Covariance matrix
    npcov = np.cov(xy)
    sigma_alpha = np.diagonal(npcov)**0.5
else:
    sigma_alpha = f['analysis']['sigma_alpha'][:]


    
    
    


fig, ax = plt.subplots( constrained_layout=True)
ax.plot(alpha_arr, Yalpha, lw=1.5, c='k', label=r' \rm{on-source:} $\mathrm{Y(\alpha) }$ ')
ax.axvline(x=1.5, ls='--', c='indianred', dashes=(5, 8), lw=1.0)

ax.set_xlabel(r"$\mathrm{\alpha}$")
ax.set_ylabel(r"$\mathrm{Y(\alpha)}$")
ax.grid(linestyle='dashed')

ax.plot(alpha_arr, mualpha, color='b', lw=0.8, ls='--', label=r' \rm{off-source:}  $\mathrm{\mu(\alpha)}$')
ax.fill_between(alpha_arr, mualpha, mualpha+sigma_alpha, color='gray', edgecolor="b", linewidth=0.0, \
                alpha=0.5, label=r'\rm{off-source:} $\mathrm{\pm \sigma(\alpha) }$')
ax.fill_between(alpha_arr, mualpha, mualpha-sigma_alpha, \
                edgecolor="b", linewidth=0.0, color='gray', alpha=0.5)
ax.set_xlim(0.3, 2.9)
ax.set_yticks( np.arange(0, args.yalpha_ymax+1) )
ax.set_ylim(args.yalpha_ymin, args.yalpha_ymax)

ax.minorticks_on()

ax.tick_params(axis='both',which='minor',length=2,width=1, direction='out')
ax.tick_params(axis='both',which='major',length=6,width=1.5, direction='out')


sub_axes = plt.axes([.68, .38, .3, .28]) 
# plot the zoomed portion
sub_axes.plot(alpha_arr, Yalpha, lw=1., c = 'k') 
sub_axes.set_ylim(args.yalpha_subax_ymin, args.yalpha_subax_ymax)
sub_axes.set_xlim(0.5, 2.5)
sub_axes.ticklabel_format(style='plain', axis='y', scilimits=(0,0), labelsize=12)

sub_axes.axvline(x=1.5, ls='--', c='indianred', dashes=(5, 8), lw=1.0)

sub_axes.tick_params(axis='both',which='minor',length=2,width=1, direction='in', labelsize=12)
sub_axes.tick_params(axis='both',which='major',length=5,width=1.3, direction='in', labelsize=12)
sub_axes.set_xticks([0.5, 1.0, 1.5, 2.0])


sub_axes.plot(alpha_arr, mualpha, color='gray', lw=0.7, ls=':', label=r' off source: $\mathrm{\mu(\alpha) }$')
sub_axes.fill_between(alpha_arr, mualpha, mualpha+sigma_alpha, color='gray', alpha=0.5, \
                edgecolor="b", linewidth=0.0, label=r'off-source: $\pm \sigma(\alpha)$')
sub_axes.fill_between(alpha_arr, mualpha, mualpha-sigma_alpha, color='gray', \
                      edgecolor="b", linewidth=0.0, alpha=0.5)


sub_axes.minorticks_on()
sub_axes.grid(linestyle='dashed')
sub_axes.grid(b=True, which='minor', color='gray', linestyle='--', lw=0.2)

#ax.ticklabel_format(style='sci', axis='y', scilimits=(0,0))

ax.legend(loc=1)
#fig.tight_layout()
figname = 'Yalpha_' + str(args.event_name) + '_1.png'
plt.savefig(figname)




#matplotlib.rcParams['font.size'] = 17

fig, ax = plt.subplots( constrained_layout=True)
ax.plot(alpha_arr, Yalpha, lw=1.5, c='k', label=r' \rm{on-source:} $\mathrm{Y(\alpha) }$ ')
ax.axvline(x=1.5, ls='--', c='indianred', dashes=(5, 8), lw=1.0)

ax.set_xlabel(r"$\mathrm{\alpha}$")
ax.set_ylabel(r"$\mathrm{Y(\alpha)}$")
ax.grid(linestyle='dashed')

ax.plot(alpha_arr, mualpha, color='b', lw=0.8, ls='--', label=r' \rm{off-source:}  $\mathrm{\mu(\alpha)}$')
ax.fill_between(alpha_arr, mualpha, mualpha+sigma_alpha, color='gray', edgecolor="b", linewidth=0.0, \
                alpha=0.5, label=r'\rm{off-source:} $\mathrm{\pm \sigma(\alpha) }$')
ax.fill_between(alpha_arr, mualpha, mualpha-sigma_alpha, \
                edgecolor="b", linewidth=0.0, color='gray', alpha=0.5)
ax.set_xlim(0.3, 2.9)


ax.minorticks_on()

ax.tick_params(axis='both',which='minor',length=2,width=1, direction='out')
ax.tick_params(axis='both',which='major',length=6,width=1.5, direction='out')


sub_axes = plt.axes([.68, .38, .3, .28]) 
# plot the zoomed portion
sub_axes.plot(alpha_arr, Yalpha, lw=1., c = 'k') 
sub_axes.set_ylim(args.yalpha_subax_ymin, args.yalpha_subax_ymax)
sub_axes.set_xlim(0.5, 2.5)
sub_axes.ticklabel_format(style='plain', axis='y', scilimits=(0,0), labelsize=12)

sub_axes.axvline(x=1.5, ls='--', c='indianred', dashes=(5, 8), lw=1.0)

sub_axes.tick_params(axis='both',which='minor',length=2,width=1, direction='in', labelsize=12)
sub_axes.tick_params(axis='both',which='major',length=5,width=1.3, direction='in', labelsize=12)
sub_axes.set_xticks([0.5, 1.0, 1.5, 2.0])


sub_axes.plot(alpha_arr, mualpha, color='gray', lw=0.7, ls=':', label=r' off source: $\mathrm{\mu(\alpha) }$')
sub_axes.fill_between(alpha_arr, mualpha, mualpha+sigma_alpha, color='gray', alpha=0.5, \
                edgecolor="b", linewidth=0.0, label=r'off-source: $\pm \sigma(\alpha)$')
sub_axes.fill_between(alpha_arr, mualpha, mualpha-sigma_alpha, color='gray', \
                      edgecolor="b", linewidth=0.0, alpha=0.5)


sub_axes.minorticks_on()
sub_axes.grid(linestyle='dashed')
sub_axes.grid(b=True, which='minor', color='gray', linestyle='--', lw=0.2)

ax.legend(loc=1)
#fig.tight_layout()

figname = 'Yalpha_' + str(args.event_name) + '_2.png'
plt.savefig(figname)





fig, ax = plt.subplots( )
ax.plot(alpha_arr, Yalpha, lw=1.5, c='k', label=r' on-source: $ Y(\alpha) $ ')
ax.axvline(x=1.5, ls='--', c='indianred', dashes=(5, 8), lw=1.0)

ax.set_xlabel(r"$\alpha$")
ax.set_ylabel(r"$Y(\alpha)$")
ax.grid(linestyle='dashed')

ax.plot(alpha_arr, mualpha, color='k', ls='--', lw=0.8, label=r' off-source:  $ \mu(\alpha)$')
ax.fill_between(alpha_arr, mualpha, mualpha+sigma_alpha, color='gray', edgecolor="b", linewidth=0.0, \
                alpha=0.5, label=r'off-source: $ \pm \sigma(\alpha) $')
ax.fill_between(alpha_arr, mualpha, mualpha-sigma_alpha, \
                edgecolor="b", linewidth=0.0, color='gray', alpha=0.5)
ax.set_xlim(0.3, 2.9)
ax.set_yticks( np.arange(0, args.yalpha_ymax+1) )
ax.set_ylim(args.yalpha_ymin, args.yalpha_ymax)

ax.minorticks_on()

ax.tick_params(axis='both',which='minor',length=2,width=1, direction='out')
ax.tick_params(axis='both',which='major',length=6,width=1.5, direction='out')


sub_axes = plt.axes([.59, .32, .3, .25]) 
# plot the zoomed portion
sub_axes.ticklabel_format(style='plain', axis='y', scilimits=(0,0), labelsize=12)
sub_axes.hist(beta33H0, density=True, bins=32, histtype='step',\
            label=r"$p( \beta \mid \mathcal{H}_0 )$")
sub_axes.axvline( x=beta33, linestyle='--', dashes=(5, 5), lw=1.0, color='red', label=r'$p(\beta \mid \mathcal{H}_3)$' )

#sub_axes.set_xlim(-5, 7)
sub_axes.minorticks_on()


#sub_axes.grid(linestyle='dashed')
#sub_axes.grid(b=True, which='minor', color='gray', linestyle='--', lw=0.2)

sub_axes.set_xlabel(r'$\beta$')
sub_axes.set_ylabel(r'$p(\beta)$')

ax.legend(loc=1)
#fig.tight_layout()
figname = 'Yalpha_' + str(args.event_name) + '_3.png'
plt.savefig(figname, bbox_inches='tight' )




fig, ax = plt.subplots( )
ax.plot(alpha_arr, Yalpha, lw=1.5, c='k', label=r' on-source: $ Y(\alpha) $ ')
ax.axvline(x=1.5, ls='--', c='indianred', dashes=(5, 8), lw=1.0)

ax.set_xlabel(r"$\alpha$")
ax.set_ylabel(r"$Y(\alpha)$")
ax.grid(linestyle='dashed')

ax.plot(alpha_arr, mualpha, color='k', ls='--', lw=0.8, label=r' off-source:  $ \mu(\alpha)$')
ax.fill_between(alpha_arr, mualpha, mualpha+sigma_alpha, color='gray', edgecolor="b", linewidth=0.0, \
                alpha=0.5, label=r'off-source: $ \pm \sigma(\alpha) $')
ax.fill_between(alpha_arr, mualpha, mualpha-sigma_alpha, \
                edgecolor="b", linewidth=0.0, color='gray', alpha=0.5)
ax.set_xlim(0.3, 2.9)
ax.set_yticks( np.arange(0, args.yalpha_ymax+1) )
ax.set_ylim(args.yalpha_ymin, args.yalpha_ymax)

ax.minorticks_on()

ax.tick_params(axis='both',which='minor',length=2,width=1, direction='out')
ax.tick_params(axis='both',which='major',length=6,width=1.5, direction='out')


sub_axes = plt.axes([.55, .33, .34, .25]) 
# plot the zoomed portion
sub_axes.ticklabel_format(style='plain', axis='y', scilimits=(0,0), labelsize=12)
sub_axes.hist(beta33H0, density=True, bins=32, histtype='step',\
            label=r"$p( \beta \mid \mathcal{H}_0 )$")
sub_axes.axvline( x=beta33, linestyle='--', dashes=(5, 5), lw=1.0, color='red', label=r'$p(\beta \mid \mathcal{H}_3)$' )

props = dict( facecolor='white', alpha=1.0, edgecolor='white')
sub_axes.text(2.5, 0.26, r'$p(\beta)$', fontsize=18, bbox=props)

#sub_axes.set_xlim(-5, 7)
sub_axes.minorticks_on()


#sub_axes.grid(linestyle='dashed')
#sub_axes.grid(b=True, which='minor', color='gray', linestyle='--', lw=0.2)

#sub_axes.set_xlabel(r'$\beta$')
#sub_axes.set_ylabel(r'$p(\beta)$')

ax.legend(loc=1)
#fig.tight_layout()
figname = 'Yalpha_' + str(args.event_name) + '_4.png'
plt.savefig(figname, bbox_inches='tight' )





# If the input file is the direct output from the inspiralhom_analysis
# then strip down the data
f = h5py.File(args.input_file, 'r') 
if f.keys()[0] != 'stripdata': 
    
    # set the file name
    if args.output_file == None:
        if args.input_file.endswith('h5'):
            outname  = args.input_file[:-3] + '_strip.h5'
        else:
            outname  = args.input_file[:-5] + '_strip.hdf5'
    else:
        outname = args.output_file
    hf = h5py.File( outname, 'w')
    STRIP = hf.create_group('stripdata')
    
    # Copy the input/ouput parameters
    f.copy('inputparams', STRIP)
    f.copy('analysis',    STRIP)
    hf['stripdata']['analysis'].create_dataset('sigma_alpha', data=sigma_alpha)
    
    cwt = STRIP.create_group('CWT')
    cwt.create_dataset('ndmfreq', data=mUtils.ndm_freq)
    
    if args.rank_analysis_file is not None:
        rf = h5py.File(args.rank_analysis_file, 'r')
        rf.copy('fix_rank_analysis',  STRIP )
        rf.copy('all_rank_analysis',  STRIP )
        

    hf.close()
else:
    f = f['stripdata']

sys.stdout.write("mass1: %s \n"  % f['inputparams']['mass1'][()] )
sys.stdout.write("mass2: %s \n"  % f['inputparams']['mass2'][()] )
sys.stdout.write("spin1z: %s \n" % f['inputparams']['spin1z'][()] )
sys.stdout.write("spin1z: %s \n" % f['inputparams']['spin2z'][()] )
sys.stdout.write("geocentric time: %s \n" % f['inputparams']['t0'][()] )
sys.stdout.write("detector time: %s \n" % f['inputparams']['l1_end_time'][()] )


