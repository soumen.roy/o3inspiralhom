\documentclass[twocolumn]{aastex63}

\usepackage{lineno}
\usepackage{color}
\usepackage{xspace}
\usepackage{amsmath}
\usepackage{acronym}

\newcommand{\eventname}{{\sc GW190412 \xspace}}
\newcommand{\EventName}{{\sc GW190412 \xspace}}

\def\GPSTime{1239082262.17\xspace}
\acrodef{GW}{gravitational-wave}


\begin{document}


\subsection{Time-frequency Tracks}

\begin{figure}
    \centering
    \includegraphics[width=0.5\textwidth]{figures/tfmap_gw190412}
    \includegraphics[width=0.49\textwidth]{figures/Yalpha_gw190412}
    \caption{\textit{Top Panel:} Time-frequency
spectrogram of data containing \EventName, observed in the LIGO-Livingston
detector. The x-axis is time (in seconds) relative to the trigger time
(\GPSTime). The amplitude scale of detector output is normalized by power
 the spectral density of the noise. The track for the $m=3$ mode is highlighted as
indicated by the plot legend, just above the dominant $m=2$ mode track.
\textit{Bottom Panel:} The variation of $Y(\alpha)$, along the "$\alpha$-th track"
defined by $f_\alpha(t) = \alpha f_{22}(t)$. The $f_{22}(t)$ track was obtained using the SEOBNRv4 waveform model. Two consecutive peaks at
$\alpha=1.0$ and $\alpha=1.5$ indicate the energy of the $m=2$ and $m=3$ modes, respectively. The track corresponding to the $m=3$ modes was obtained by multiplying the $(2,2)$ track by $\alpha=1.5$ as indicated in the top panel. The value of $\alpha$ for the $m = 3$ modes is marked with the red dashed vertical line.
\textit{Inset}: A zoomed version of the peak at $\alpha=1.5$, corresponding to
the $m=3$ modes. 
%A similar analysis on different off-source data segments allows
%us to calculate ensemble average $\mu(\alpha)$ and variance $\sigma$ of $N(\alpha)$ due to detector noise. These are plotted to demonstrate the
%relative strength of the various modes of the signal present in the on-source segment.
For comparison, we show the ensemble average $\mu(\alpha)$ and standard deviation $\sigma$ of $Y(\alpha)$ due to detector noise, computed off-source.
}
    \label{fig:detection_higher_modes_inspiral}
\end{figure}


An independent analysis was performed using the time-frequency representation of
the data to detect the presence of higher-order multipoles in the inspiral part
of
the signal, as outlined in \cite{roy2019unveiling}.
The instantaneous frequency $f_{\ell m}(t)$ of the \ac{GW} signal
from an inspiraling compact binary is related to the that of the
dominant $(2,2)$ mode by $f_{\ell m}(t) \simeq (m/2) \;
f_{22}(t)$. We use the maximum likelihood source parameters from standard
analysis using SEOBNRv4HM and IMRPhenomHM waveform models presented
in Sec.~\ref{sec:source_properties} to define $f_{22}(t)$.
%

Inspired by the above scaling relation, we then look along the generalized
tracks, $f_\alpha(t) \simeq \alpha f_{22}(t)$, in a time-frequency representation
that is the absolute square of the
continuous wavelet transformation (CWT) of the whitened on-source data, $\tilde X (t,f)$. 
We have used Morelet wavelets to perform the CWT, where the central frequency of the wavelet was
chosen so as to maximize the sum of pixel values along the $f_{22}(t)$ curve. 
This wavelet transformation is shown in the top panel of Fig.~\ref{fig:detection_higher_modes_inspiral}.

In order to quantify the energy along each track, we define $Y(\alpha)$ to be
the energy contained in the pixels of $|\tilde X (t,f)|^2 $ containing the track $f_\alpha(t)$, 
where the pixel size is $\Delta t = 1/4096\, s$ along the time-axis and $\Delta f = 1/5\,\text{Hz}$ along the frequency axis, corresponding to a sampling rate of 4 kHz, and analysis time window of $5$ seconds respectively,
%the sum of pixels of
%$\tilde X (t, f_\alpha)$ which lie along the $\alpha$-th track, i.e.,
%\begin{equation}
%\displaystyle Y(\alpha) = \sum_{t=-\Delta t}^{t_{\rm ISCO}} \tilde{X} (t, f =
%\alpha f_{22}(t)),
%\end{equation}
%
%where $\Delta t$ is chosen to be $0.5\,s$ and $t_{\rm ISCO}$ denotes the time
%when orbiting masses reach the innermost stable circular orbit. 
By doing so, we
can decouple the energy in individual modes of the signal as shown in bottom
panel of Fig.~\ref{fig:detection_higher_modes_inspiral}.
%
$Y(\alpha)$ is seen to
have a global peak at $\alpha = 1$, corresponding to the dominant $(2,2)$ mode,
and a prominent local peak at $\alpha = 1.5$, corresponding to the $m = 3$
modes [Bottom panel on
Figure~\ref{fig:detection_higher_modes_inspiral}].


%One can also calculate $Y(\alpha) \equiv N(\alpha)$ from different off-source
%data segments to capture the detector noise characteristics. 

We also calculate $Y(\alpha)$ 
from different off-source data segments to capture the detector noise characteristics; in this case we call the quantity $N(\alpha)$.
Their ensemble average $\mu(\alpha) = \langle N(\alpha) \rangle$ and covariance $\sigma (\alpha)$ 
are also plotted for reference and for highlighting the relative strength of the GW signal present in the on-source segment.

We can further test the hypotheses that the data contained either only noise
($\mathcal H_0$), or noise and a dominant-multipole maximum likelihood signal
($\mathcal H_1$), or noise and a maximum likelihood signal that includes $m=2$
and $m=3$ multipoles ($\mathcal H_2$). By maximizing the likelihood of
observing $Y(\alpha)$ given each hypothesis over a free amplitude parameter for
each multipole, we obtain likelihood ratios that in turn can be incorporated
into a detection statistic $\beta$ (see Eq.~(7) in \cite{roy2019unveiling}).

From the on-source data segment taken from LIGO Livingston detector, we found the detection statistic $\beta = 6.5$ with a $p-$value of $ 6.4\times10^{-4}$ for SEOBNRv4HM model; and $\beta = 7.5$ with a $p-$value $ < 6.4\times10^{-4}$ for IMRPhenomHM model, which strongly supports the presence of $m=3$ modes in the signal. The highest values of $\beta$ from off-sources data samples were found to be $7.2$ and $6.0$ respectively. 
The significance of this event was determined using the background distribution $p(\beta \mid \mathcal{H}_{0})$
from off-source data segments from LIGO Livingston detector surrounding the trigger time of \EventName.


  
%\begin{figure}
%    \centering
%    \includegraphics[width=0.5\textwidth]{figures/tfmap_S190412m_C01}
%    \includegraphics[width=0.5\textwidth]{figures/Yalpha_S190412m_C01}
%    \caption{\textcolor{red}{PRELIMINARY}\textit{Top Panel:} Time-frequency spectrogram of data containing \eventname, observed in the LIGO-Livingston detector. The x-axis is time (in seconds) relative to the trigger time (GPSTIME). The amplitude scale of detector output is normalized by power spectral density of the noise. The track for the $m=3$ mode is highlighted as indicated by the plot legend, just above the dominant $m=2$ mode track. \textit{Bottom Panel:} The variation of $Y(\alpha)$, i.e, the energy summed over all the pixels along a given $\alpha$-track, as a function of $\alpha$. Two consecutive peaks of $Y(\alpha)$ at $\alpha=1.0$ and $\alpha=1.5$ indicate the energy of the $m=2$ and $m=3$ mode, respectively. \textit{Inset}: A zoomed version of the peak at $\alpha=1.5$, corresponding to the m=3 mode.}
%    \label{fig:detection_higher_modes_inspiral}
%\end{figure}
%
%
%An independent analysis was performed using the time-frequency representation of the data to detect the presence of higher-order modes in the inspiral part of the signal, as outlined in ~\cite{roy2019unveiling}. The method is based on the fact that the instantaneous frequency of each mode of the signal is related to the dominant $(2, 2)$ mode: $f_{\ell m}(t;\vec{\lambda}) = m/2 \; f_{22}(t;\vec{\lambda})$. This relation allows to define an arbitrary time-frequency track that is scaled to the trajectory of the $(2, 2)$ mode:
%\begin{equation}
%    f_\alpha(t;\vec{\lambda}) = \alpha f_{22}(t;\vec{\lambda}),
%\end{equation}
%where $\alpha$ refers to the scaling factor. The $(2, 2)$ mode track is parametrized by a set of intrinsic parameters, $\vec{\lambda}$, such as component masses and spins. Using a time-frequency spectrogram of the signals, this relation is leveraged for evaluating the signal energy along such scaled tracks, thereby decoupling the energies of the individual modes of the signal. This mapping calculate for the on-source data segment, $Y(\alpha)$, is illustrated in the top panel of Fig.~\ref{fig:detection_higher_modes_inspiral}, where the $(2, 2)$  track is determined using the masses and spins of the best-fit GR waveform from the quasi-circular inspiral of the compact binary system. $Y(\alpha)$ is seen to have a global peak at $\alpha = 1$, corresponding to the dominant $(2,2)$ mode, and a prominent local peak at $\alpha = 1.5$, corresponding to the $m = 3$ modes [Inset of bottom panel on Figure \ref{fig:detection_higher_modes_inspiral}].
%
%
%
%While the data model $Y(\alpha)$ is constructed from the on-source data; the template model $S(\alpha)$ is constructed from the time-frequency map of the best-fit GR waveform. For detecting  higher-order modes, we assume two hypotheses:
%\begin{equation}
%\label{eq:DefHypotheses}
%  \begin{split}
%    \mathcal{H}_1 : Y(\alpha) &= N(\alpha) + a_2 S_2(\alpha),  \\
%    \mathcal{H}_2 : Y(\alpha) &= N(\alpha) + a_2 S_2(\alpha) + a_3 S_2(\alpha), 
%  \end{split}
%\end{equation} 
%where,  $N(\alpha)$ is the contribution from detector noise, $S_2(\alpha)$ and $S_3(\alpha)$ are two template models containing $m=2$ and $m=3$ modes respectively. 
%$\mathcal{H}_1$ implies that the data model $Y(\alpha)$ contains only the contribution from the $m=2$ modes embedded in detector boise, whereas $\mathcal{H}_2$ implies that in addition, there are contributions from the $m=3$ modes as well. The quantities $a_2$ and $a_3$ are two unknown overall amplitudes that are determined by maximizing the log-likelihood ratios:
%\begin{equation}
%\Lambda_{1,2} = \max_{a_2, a_3}\, \ln \left[ \frac{p\left(Y(\alpha) \mid \mathcal{H}_{1,2}\right)}{p\left(Y(\alpha) \mid \mathcal{H}_{0}\right)} \right], 
%\end{equation}
%against the the null-hypothesis $\mathcal{H}_{0} : Y(\alpha) = N(\alpha)$ which asserts that $Y(\alpha)$ is contributed by detector noise alone. For the on-source segment, $\Lambda_1$ and $\Lambda_2$ were found to be $1457.1$ and $1471.8$ respectively, which implies that the model with $m=2,3$ modes is more favorable over the one with $m=2$ mode only.
%%
%A detection statistic $\beta$ is defined in terms of $\Lambda_1$ and $\Lambda_2$ to indicate the strength of $m=3$ modes in $Y(\alpha)$ as:
%\begin{equation}
%  \beta = \left( \Lambda_2 - \Lambda_1 + \gamma_3^2/2 \right)/\gamma_3,
%\end{equation}
% where, $\gamma_3 = \lVert a_3 S_3(\alpha)\rVert$.
%%
%It can be shown that in the presence of ideal Gaussian noise, $p(\beta \mid \mathcal{H}_{0}) \equiv \mathcal{N}(0,1)$. 
%
%We have determined the background distribution $p(\beta \mid \mathcal{H}_{0})$ from off-source data segments surrounding the trigger time of \eventname to be zero-mean Gaussian with a variance of $1.3^2$. From the on-source data segment, we found $\beta = 5.42$ with a $p-$value  $\sim 1.8 \times 10^{-5}$, which strongly indicates the presence of $m=3$ modes in the signal. 



\bibliographystyle{aasjournal}
\bibliography{references}

\end{document}
