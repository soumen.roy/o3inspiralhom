## Link of the summary statistics page
## https://galahad.aei.mpg.de/~rcotesta/LVC/offline_pe/S190412m/final_results_review/SEOBNRv4_ROM_nest/samples/posterior_samples.json

python inspiralhom_analysis.py --detector L1 \
            --frame-type L1_HOFT_C00 --channel-name L1:GDS-CALIB_STRAIN \
            --window-width 34000 --number-threads 70 \
            --approximant SEOBNRv4HM \
            --track-end-frequency 0.6fISCO \
            --track-max-length 0.45 \
            --mass1 31.498191603298906 --mass2 10.262950698389067 \
	    --spin1z 0.18194263211717318 --spin2z 0.3339564998934089 \
	    --event-time 1239082262.1848493 \
	    --det-event-time 1239082262.1655934 \
            --output-filename S190412m_L1_C00_fISCO_0p45.h5
