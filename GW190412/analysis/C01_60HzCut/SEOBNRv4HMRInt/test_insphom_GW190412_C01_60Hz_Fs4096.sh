## Link of the summary statistics page
## https://git.ligo.org/pe/O3/S190412m/-/blob/master/Preferred/dat/SEOBNRv4HM_ROM_pesummary.dat
## https://git.ligo.org/pe/O3/S190412m/-/blob/master/Preferred/config_files/SEOBNRv4HM_ROM/config.ini

## Link of science segment and veto segment file
## https://ldas-jobs.ligo.caltech.edu/~charlie.hoy/o3/runs/hl/c01/a2_initial/1._analysis_time/1.01_segment_data/

python inspiralhom_analysis.py --detector L1 \
            --frame-type L1_HOFT_CLEAN_SUB60HZ_C01 --channel-name L1:DCS-CALIB_STRAIN_CLEAN_SUB60HZ_C01 \
            --science-segment-file H1L1-SCIENCE-1238787802-853441.xml \
            --veto-segment-file H1L1-VETOES-1238787802-853441.xml \
            --window-width 24000 --number-threads 45 \
            --approximant SEOBNRv4HM \
            --track-end-frequency 0.6fISCO \
            --track-max-length 0.45 \
            --mass1 34.60826511271556 --mass2 9.371097686383452 \
            --spin1z 0.3980689407796103 --spin2z -0.6504589485668013 \
            --event-time 1239082262.1809337 \
            --det-event-time 1239082262.1614335 \
            --deltaT 0.000244140625 \
            --output-filename gw190412_L1_C01_60Hz_4096_0p6fISCO_0p45.h5
