## Link of the parameter estimation page
##  https://git.ligo.org/pe/O3/S190412m/-/raw/master/Preferred/dat/A4-C01Sub60-PhenomHM.dat
## Link of configuration file
## https://ligo.gravity.cf.ac.uk/~lionel.london/LVC/offline_pe/O3/S101412m/C01/A-C00-C01-Comp/A4-C01Sub60-PhenomHM/config.ini 

## Link of science segment and veto segment file
## https://ldas-jobs.ligo.caltech.edu/~charlie.hoy/o3/runs/hl/c01/a2_initial/1._analysis_time/1.01_segment_data/

python inspiralhom_analysis.py --detector L1 \
            --frame-type L1_HOFT_CLEAN_SUB60HZ_C01 --channel-name L1:DCS-CALIB_STRAIN_CLEAN_SUB60HZ_C01 \
            --science-segment-file ../H1L1-SCIENCE-1238787802-853441.xml \
            --veto-segment-file ../H1L1-VETOES-1238787802-853441.xml \
            --window-width 24000 --number-threads 50 \
            --approximant IMRPhenomHM \
            --track-end-frequency 0.6fISCO \
            --track-max-length 0.45 \
            --mass1 31.87245441757187 --mass2 10.06699145617058 \
            --spin1z 0.24077308698449598 --spin2z -0.03284343587462677 \
            --event-time 1239082262.185383 \
            --det-event-time 1239082262.1658118 \
            --deltaT 0.000244140625 \
            --output-filename gw190412_L1_C01_60Hz_4096_0p6fISCO_0p45.h5
