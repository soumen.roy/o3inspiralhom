import os
import numpy as np

# web link: ## https://git.ligo.org/pe/O3/S190412m/-/blob/master/Preferred/dat/SEOBNRv4HM_ROM_pesummary.dat


f = open('../M4-PhenomHM_pesummary.dat', 'r')
data = []
for line in f.readlines():
    data.append(line.split() )
    
param_names = [x for x in data[0]]
samples = []
for ii in range(1, len(data)):
    samples.append([ float(data[ii][jj]) for jj in range( len(data[ii]) ) ]  )
samples = np.array(samples)

indx = np.where( samples[ :,param_names.index('log_likelihood') ] == \
                np.max( samples[ :,param_names.index('log_likelihood') ] ) )[0][0]


mass1 = samples[ :,param_names.index('mass_1') ][indx] 
mass2 = samples[ :,param_names.index('mass_2') ][indx] 
spin1z = samples[ :,param_names.index('spin_1z') ][indx] 
spin2z = samples[ :,param_names.index('spin_2z') ][indx] 
geotime = samples[ :,param_names.index('geocent_time') ][indx]

dtime = samples[ :,param_names.index('L1_time') ][indx] 


term1 = "python inspiralhom_analysis.py --detector L1 --frame-type L1_HOFT_CLEAN_SUB60HZ_C01" + \
         " --channel-name L1:DCS-CALIB_STRAIN_CLEAN_SUB60HZ_C01 " + \
            " --science-segment-file H1L1-SCIENCE-1238787802-853441.xml" +  \
            " --veto-segment-file H1L1-VETOES-1238787802-853441.xml " + \
            " --window-width 15200 --number-threads 50 " + \
            " --approximant IMRPhenomHM " + \
            " --track-end-frequency 0.6fISCO " + \
            " --track-max-length 0.45 " + \
            " --ndm-freq 14.391830836484655 " 
        


sorted_indices = np.argsort( samples[ :,param_names.index('log_likelihood') ] )[::-1]

for ii in range(101, 201):
    
    indx = sorted_indices[ii]
    
    mass1 = samples[ :,param_names.index('mass_1') ][indx] 
    mass2 = samples[ :,param_names.index('mass_2') ][indx] 
    spin1z = samples[ :,param_names.index('spin_1z') ][indx] 
    spin2z = samples[ :,param_names.index('spin_2z') ][indx] 
    geotime = samples[ :,param_names.index('geocent_time') ][indx]
    dtime = samples[ :,param_names.index('L1_time') ][indx]
    
    term2 = " --event-time %s "%geotime + " --det-event-time %s " %dtime + \
            " --mass1 %s "% mass1 + " --mass2 %s "% mass2 + \
            " --spin1z %s "%spin1z + " --spin2z %s "%spin2z
                
    term3 = " --deltaT 0.000244140625 --output-filename data/gw190412_L1_C01_60Hz_4096_0p6fISCO_0p45_%s.h5 "%str(ii)
    
    script = term1 + term2 + term3 
    os.system( script )
                
