python read_insphom_results.py --input-file gw190412_L1_C01_60Hz_4096_0p6fISCO_0p45_strip.h5 \
                        --matplotlib-params-style-file MATPLOTLIB_RCPARAMS.sty \
                        --event-name gw190412 \
                        --tfmap-tmin -1.0 \
                        --tfmap-fmax 200.0 \
                        --yalpha-subax-ymin 0.15 \
                        --yalpha-subax-ymax 0.95 \
                        --yalpha-ymax 7.4
