## Link of the summary statistics page
## https://galahad.aei.mpg.de/~rcotesta/LVC/offline_pe/S190412m/final_results_review/SEOBNRv4_ROM_nest/samples/posterior_samples.json

python inspiralhom_analysis.py --detector L1 \
            --frame-type L1_HOFT_C01 --channel-name L1:DCS-CALIB_STRAIN_CLEAN_C01 \
            --window-width 34000 --number-threads 35 \
            --approximant SEOBNRv4HM \
            --track-end-frequency 0.6fISCO \
            --track-max-length 0.45 \
            --mass1 33.76798828818286 --mass2 9.700004180482646 \
            --spin1z 0.28558620162670567 --spin2z 0.03826741661275385 \
            --event-time 1239082262.181084 \
            --det-event-time 1239082262.1614943 \
            --deltaT 0.000244140625 \
            --output-filename gw190412_L1_C01_4096_0p6fISCO_0p45.h5
