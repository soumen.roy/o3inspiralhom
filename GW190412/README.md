# Analysis on the inspiral higher-order mode detection from GW190412

GW190412 was observed on 12-04-2019 at 05:30:44 UTC. The FAR for this event was 1.683e-27, with a matched filter network SNR of 18.2125. Click [here](https://gracedb.ligo.org/events/G329483/view/) for more details.

