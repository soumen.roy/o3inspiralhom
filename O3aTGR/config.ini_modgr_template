[analysis]
ifos = ['H1', 'L1', 'V1']
engine = lalinferencenest
nparallel = 4
service-url = https://gracedb.ligo.org/api/
roq = False
coherence-test = False
upload-to-gracedb = False
singularity = False
osg = False
dataseed = 1000

[paths]
webdir = 
roq_b_matrix_directory = /home/cbc/ROQ_data/PhenomPv2
basedir = 
daglogdir = 

[input]
max-psd-length = 10000
padding = 16
events = all
gps-time-file = /home/rahul.kashyap/o3_highermodes/injection_o3HMinsp/injection_runs/mod-gr_hom/gpstime.txt
analyse-all-time = False
timeslides = False
ignore-gracedb-psd = True
ignore-state-vector = True
gps-start-time = 1249852250
gps-end-time = 1249852260

[condor]
datafind = /bin/true
mergeNSscript = /home/ish.gupta/opt/lalsuite_tgrhm_latest/bin/lalinference_nest2pos
mergeMCMCscript = /home/ish.gupta/opt/lalsuite_tgrhm_latest/bin/cbcBayesMCMC2pos
combinePTMCMCh5script = /home/ish.gupta/opt/lalsuite_tgrhm_latest/bin/cbcBayesCombinePTMCMCh5s
resultspage = /home/ish.gupta/opt/lalsuite_tgrhm_latest/bin/cbcBayesPostProc
segfind = /home/ish.gupta/opt/lalsuite_tgrhm_latest/bin/ligolw_segment_query
ligolw_print = /home/ish.gupta/opt/lalsuite_tgrhm_latest/bin/ligolw_print
coherencetest = /home/ish.gupta/opt/lalsuite_tgrhm_latest/bin/lalinference_coherence_test
lalinferencenest = /home/ish.gupta/opt/lalsuite_tgrhm_latest/bin/lalinference_nest
lalinferencemcmc = /home/ish.gupta/opt/lalsuite_tgrhm_latest/bin/lalinference_mcmc
lalinferencebambi = /home/ish.gupta/opt/lalsuite_tgrhm_latest/bin/lalinference_bambi
lalinferencedatadump = /home/ish.gupta/opt/lalsuite_tgrhm_latest/bin/lalinference_datadump
ligo-skymap-from-samples = /home/ish.gupta/opt/lalsuite_tgrhm_latest/bin/ligo-skymap-from-samples
ligo-skymap-plot = /home/ish.gupta/opt/lalsuite_tgrhm_latest/bin/ligo-skymap-plot
processareas = /home/ish.gupta/opt/lalsuite_tgrhm_latest/bin/process_areas
computeroqweights = /home/ish.gupta/opt/lalsuite_tgrhm_latest/bin/lalinference_compute_roq_weights
mpiwrapper = /home/ish.gupta/opt/lalsuite_tgrhm_latest/bin/lalinference_mpi_wrapper
gracedb = /bin/true
ppanalysis = /home/ish.gupta/opt/lalsuite_tgrhm_latest/bin/cbcBayesPPAnalysis
pos_to_sim_inspiral = /home/ish.gupta/opt/lalsuite_tgrhm_latest/bin/cbcBayesPosToSimInspiral
mpirun = /ldcg/intel/2018u3/compilers_and_libraries_2018.3.222/linux/mpi/intel64/bin/mpirun
accounting_group = ligo.prod.o3.cbc.testgr.tiger
accounting_group_user = rahul.kashyap
sharedfs = True

[datafind]
url-type = file
types = {'H1': 'HWINJ_INJECTED', 'L1': 'HWINJ_INJECTED', 'V1': 'HWINJ_INJECTED'}

[data]
channels = {'H1': 'H1:HWINJ_INJECTED', 'L1': 'L1:HWINJ_INJECTED', 'V1': 'V1:HWINJ_INJECTED'}

[lalinference]
flow = {'H1': 40.0, 'L1': 40.0, 'V1': 40.0}
fake-cache = {'H1':'interp:/home/ish.gupta/opt/lalsuite_tgrhm_latest/share/lalsimulation/LIGO-T1800545-v1-aLIGO_O3low.txt','L1':'interp:/home/ish.gupta/opt/lalsuite_tgrhm_latest/share/lalsimulation/LIGO-T1800545-v1-aLIGO_O3low.txt','V1':'interp:/home/ish.gupta/opt/lalsuite_tgrhm_latest/share/lalsimulation/LIGO-T1800545-v1-AdV_O3low.txt'}

[engine]
fref = 40.0
approx = IMRPhenomHMpseudoFourPN
amporder = 0
neff = 1000
nlive = 512
tolerance = 0.1
ntemps = 16
resume = 
adapt-temps = 
progress = 
hmParams = 
distance-max = 500
q-min = 0.08
q-max = 0.9
q_hm-min = 0.08
q_hm-max = 0.9
alignedspin-zprior = 
srate = 4096.0
seglen = 8.0
chirpmass-min = 2.0
chirpmass-max = 30.0
chirpmass_hm-min = 2.0
chirpmass_hm-max = 30.0
comp-min = 1.0
comp-max = 75.0
no-detector-frame = 
fix-rightascension = 0
fix-declination = 0
fix-polarisation = 1.5708
fix-phase = 0.349
H1-psd = /home/carl-johan.haster/projects/O3/S190814bv/makeBW/H1/post/clean/glitch_median_PSD_forLI_H1.dat
L1-psd = /home/carl-johan.haster/projects/O3/S190814bv/makeBW/L1/post/clean/glitch_median_PSD_forLI_L1.dat
V1-psd = /home/carl-johan.haster/projects/O3/S190814bv/makeBW/V1small/post/clean/glitch_median_PSD_forLI_V1.dat

[mpi]
mpi_task_count = 16
machine-count = 16
machine-memory = 4000

[skyarea]
maxpts = 2000

[resultspage]
skyres = 0.5

[bayeswave]

[statevector]
state-vector-channel = {'H1': 'H1:GDS-CALIB_STATE_VECTOR', 'L1': 'L1:GDS-CALIB_STATE_VECTOR', 'V1': 'V1:DQ_ANALYSIS_STATE_VECTOR'}
bits = ['Bit 0', 'Bit 1', 'Bit 2']

[merge]

[ppanalysis]

[singularity]

[ligo-skymap-from-samples]
enable-multiresolution = 

[ligo-skymap-plot]
annotate = 
contour = 50 90

