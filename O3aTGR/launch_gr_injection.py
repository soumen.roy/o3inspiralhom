!/usr/bin/env python
## RahulKashyap/17.Mar.2020
##
# -*- coding: utf-8 -*-
__author__ = "Rahul Kashyap","Ish Mohan Gupta"
__email__ = "rkk5314@psu.edu"
__status__ = "Development"

"""
launches a set of runs by creating a set of injection corresponding to masses of BBH 
requires config.ini_template
current runs are in /home/rahul.kashyap/o3_highermodes/injection_o3HMinsp/injection_runs/gr_hom
"""

import os
import numpy as np 

template_file = 'config.ini_template'
path="/home/rahul.kashyap/public_html/o3highermodes_results/injection_o3HMinsp/injection_runs/gr_hom"
masses=[[24.36,2.72],[27.71,2.51],[21.59,14.15],[22.88,13.25],[22.97,2.61],[24.39,2.52],[17.98,3.01],[33.53,1.99]]

for i,m1m2 in enumerate(masses[0:5]):

	os.system("mkdir -p run_%02d"%i)
	os.chdir("%s/run_%02d"%(path,i))

	print m1m2[0],m1m2[1]
	os.system("lalapps_inspinj --output injection.xml --seed 5227 --f-lower 20 \
                --waveform IMRPhenomHMpseudoFourPN \
                --gps-start-time 1249852257 --gps-end-time 1249852267 \
                --t-distr uniform --time-step 10 --time-interval 20 \
                --d-distr uniform --l-distr fixed  \
                --min-distance 243000 --max-distance 243000 \
                --i-distr fixed --polarization 96.53 --fixed-inc 48.37 \
                --coa-phase-distr fixed --fixed-coa-phase 19.75 \
                --m-distr fixMasses \
                --fixed-mass1 %f --fixed-mass2 %f \
                --taper-injection startend --enable-spin --aligned --amp-order 0 \
                --longitude 13.1625 --latitude -24.16 \
                --min-spin1 0.062366 --max-spin1 0.062366 --min-spin2 0.370941 --max-spin2 0.370941"%(m1m2[0],m1m2[1]))

    	with open(path+'/'+template_file, 'r') as file:
        	# read a list of lines into data
        	data = file.readlines()

    	print data[13],data[15],data[16]
    	data[13] = "webdir = %s/run_%02d\n"%(path,i) 
    	data[15] = "basedir = %s/run_%02d\n"%(path,i)
    	data[16] = "daglogdir = %s/run_%02d/daglog\n"%(path,i)
	data[22] = "injection-file = %s/run_%02d/injection.xml\n"%(path,i)

    	print data[13],data[15],data[16],data[22]

    	# and write everything back
    	with open(path+'/run_%02d/config.ini'%i, 'w') as file:
        	file.writelines(data)

    	
    	print os.getcwd()

	os.system("lalinference_pipe -r %s/run_%02d -p %s/run_%02d/daglog config.ini"%(path,i,path,i))

	os.system('condor_submit_dag %s/run_%02d/multidag.dag'%(path,i))
	os.chdir("%s"%path)


