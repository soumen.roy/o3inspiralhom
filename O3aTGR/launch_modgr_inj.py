!/usr/bin/env python
## RahulKashyap/17.Mar.2020
##
# -*- coding: utf-8 -*-
__author__ = "Rahul Kashyap","Ish Mohan Gupta"
__email__ = "rkk5314@psu.edu"
__status__ = "Development"

"""
creates and launches the runs corresponding to different masses for 22 and higher modes.
requires config.ini_template in a specific format and gpstime.txt
current runs are in /home/rahul.kashyap/o3_highermodes/injection_o3HMinsp/injection_runs/mod-gr_hom

"""

import os, re, commands, glob
import numpy as np

def write_cache(frame, cache_folder=None):
  """
  Writes cache for frame with a single channel.

  Parameters
  ----------
  frame : frame file
  cache_folder : output folder to write file to

  Returns
  -------
  A blank-separated string (needs to be split):
  'cachefilename ch channel start_time datalen realpath'
  """
  frame_dump = commands.getoutput('FrDump -i %s'%(frame))
  start_time = int(re.findall('start at:[0-9]* ', frame_dump)[0][9:-1])
  datalen = int(re.findall('length=[0-9]*\.', frame_dump)[0][7:-1])
  channel = re.findall('ProcData:.*', frame_dump)[0][10:]
  realpath = os.path.realpath(frame)
  cachefilename = '%s-%s_CACHE-%d-%d.lcf'%(channel[0], channel[3:], start_time, datalen+16)
  if cache_folder is not None:
    os.system('mkdir -p %s'%(cache_folder))
    ofile = open(os.path.join(cache_folder, cachefilename), 'w')
    ofile.write('%s %s:%s %d %d file://localhost%s\n'%(channel[0], channel[:2], channel[3:], start_time, datalen, realpath))
    ofile.close()
  return '%s %s %s %d %d file://localhost%s'%(cachefilename, channel[0], channel, start_time, datalen, realpath)

#write_cache('H1-HWINJ.gwf','.')
#write_cache('L1-HWINJ.gwf','.')
#write_cache('V1-HWINJ.gwf','.')


template_file = 'config.ini_template'
path="/home/rahul.kashyap/public_html/o3highermodes_results/injection_o3HMinsp/injection_runs/mod-gr_hom"
#path="/home/rahul.kashyap/o3_highermodes/injection_o3HMinsp/injection_runs/mod-gr_hom"
masses=[[24.36,2.72],[27.71,2.51],[21.59,14.15],[22.88,13.25],[22.97,2.61],[24.39,2.52],[17.98,3.01],[33.53,1.99]]
iota=[10,60,10,60,25,120,50]
epsilon=[0.2,0.5,0.2,0.5,0.3,0.1]
for i,m1m2 in enumerate(masses[0:5]):

	os.system("mkdir -p run_%02d"%i)
	os.chdir("%s/run_%02d"%(path,i))

	m1=m1m2[0]; m2=m1m2[1]
	print m1,m2
	chirpmass_hm = (((m1*m2)**3/(m1+m2))**(1./5.))*(1+epsilon[i])
	q_hm = (m2/m1)*(1+epsilon[i])

	os.system("pycbc_generate_hwinj --approximant IMRPhenomHM  --order pseudoFourPN --mass1 %f --mass2 %f --chirpmass_hm %f  --q_hm %f --inclination %f --polarization 90 --ra 0 --dec 0 --network-snr 28.5 --geocentric-end-time 1249852257 --gps-start-time 1249852250 --gps-end-time 1249852260 --instruments H1 L1 V1 --asd-file H1:/home/ish.gupta/src/tgrhm_env_pycbc/opt/lalsuite/share/lalsimulation/LIGO-T1800545-v1-aLIGO_O3low.txt L1:/home/ish.gupta/src/tgrhm_env_pycbc/opt/lalsuite/share/lalsimulation/LIGO-T1800545-v1-aLIGO_O3low.txt V1:/home/ish.gupta/src/tgrhm_env_pycbc/opt/lalsuite/share/lalsimulation/LIGO-T1800545-v1-AdV_O3low.txt  --taper TAPER_STARTEND  --waveform-low-frequency-cutoff 20.0 --low-frequency-cutoff 20.0 --sample-rate 4096"%(m1,m2,chirpmass_hm,q_hm,iota[i]))

	print 'txt files in current dir',os.getcwd(),glob.glob('*H1.txt')[0]


	H1INJ_FILE = glob.glob('*H1.txt')[0]   #"hwinjcbc_1249852249_H1.txt"
	#print H1INJ_FILE
	os.system("pycbc_insert_frame_hwinj --frame-type H1_HOFT_C00 --channel-name H1:GDS-CALIB_STRAIN_CLEAN --gps-start-time 1249852234 --gps-end-time 1249852276 --pad-data 8 --strain-high-pass 30.0 --sample-rate 4096 --hwinj-file %s/run_%02d/%s --hwinj-start-time 1249852248 --ifo H1 --output-file %s/run_%02d/H1-HWINJ.gwf"%(path,i,H1INJ_FILE,path,i))

	L1INJ_FILE = glob.glob('*L1.txt')[0]   #"hwinjcbc_1249852249_L1.txt"
	os.system("pycbc_insert_frame_hwinj --frame-type L1_HOFT_C00 --channel-name L1:GDS-CALIB_STRAIN_CLEAN --gps-start-time 1249852234 --gps-end-time 1249852276 --pad-data 8 --strain-high-pass 30.0 --sample-rate 4096 --hwinj-file %s/run_%02d/%s --hwinj-start-time 1249852248 --ifo L1 --output-file %s/run_%02d/L1-HWINJ.gwf"%(path,i,L1INJ_FILE,path,i))

	V1INJ_FILE = glob.glob('*V1.txt')[0]    #"hwinjcbc_1249852249_V1.txt"
	os.system("pycbc_insert_frame_hwinj --frame-type V1Online --channel-name V1:Hrec_hoft_16384Hz --gps-start-time 1249852234 --gps-end-time 1249852276 --pad-data 8 --strain-high-pass 30.0 --sample-rate 4096 --hwinj-file %s/run_%02d/%s --hwinj-start-time 1249852248 --ifo V1 --output-file %s/run_%02d/V1-HWINJ.gwf"%(path,i,V1INJ_FILE,path,i))

    	with open(path+'/'+template_file, 'r') as file:
        	# read a list of lines into data
        	data = file.readlines()

    	print data[13],data[15],data[16]
    	data[13] = "webdir = %s/run_%02d\n"%(path,i) 
    	data[15] = "basedir = %s/run_%02d\n"%(path,i)
    	data[16] = "daglogdir = %s/run_%02d/daglog\n"%(path,i)

    	print data[13],data[15],data[16]

    	# and write everything back
    	with open(path+'/run_%02d/config.ini'%i, 'w') as file:
        	file.writelines(data)

    	
    	print os.getcwd()

	os.system("lalinference_pipe -r %s/run_%02d -p %s/run_%02d/daglog config.ini"%(path,i,path,i))
	write_cache('%s/run_%02d/H1-HWINJ.gwf'%(path,i),'%s/run_%02d/cache/'%(path,i))
	write_cache('%s/run_%02d/L1-HWINJ.gwf'%(path,i),'%s/run_%02d/cache/'%(path,i))
	write_cache('%s/run_%02d/V1-HWINJ.gwf'%(path,i),'%s/run_%02d/cache/'%(path,i))

	os.system('condor_submit_dag %s/run_%02d/multidag.dag'%(path,i))
	os.chdir("%s"%path)


