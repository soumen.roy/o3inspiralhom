#!/usr/bin/env python
## RahulKashyap/11.Mar.2020
## 
# -*- coding: utf-8 -*-
__author__ = "Rahul Kashyap"
__email__ = "rkk5314@psu.edu"
__status__ = "Development"

"""Caclulate the difference between alpha values corresponding to two peaks near alpha=1 and alpha=1.5 for a sample of Y(alpha)       curves. 
"""

import h5py
import glob
import numpy as np
import matplotlib.pyplot as plt
from scipy import stats


path="/home/soumen.roy/HHM/o3inspiralhom/inspiral_hom_contribution/GW190412/C01/test_dfdt/test5/onsource_beta0814/data/"
#path="/home/soumen.roy/HHM/o3inspiralhom/inspiral_hom_contribution/GW190412/C01/test_dfdt/test5/data/"
filelist=glob.glob(path+'*.h5')



delta_alpha_23=[]
for i,file in enumerate(filelist[0:299]):
    #print file
    f=h5py.File(file,'r')
    ds=np.array(f['analysis']['Yalpha'])

    alpha=f['analysis']['alpha_arr'][:]
    #print len(alpha)
    #print f['analysis']['Yalpha'][140:180]
    i_22max=160+np.argmax(f['analysis']['Yalpha'][141:181],axis=0)
    #print i_22max
    #print f['analysis']['alpha_arr'][i_22max], f['analysis']['Yalpha'][i_22max]
    
    i_33max=270+np.argmax(f['analysis']['Yalpha'][251:291],axis=0)
    #print i_33max
    #print f['analysis']['alpha_arr'][i_33max], f['analysis']['Yalpha'][i_33max]
    
    delta_alpha_23.append(f['analysis']['alpha_arr'][i_33max]-f['analysis']['alpha_arr'][i_22max])
    
    plt.plot( f['analysis']['alpha_arr'][:], f['analysis']['Yalpha'][:] )
     
#np.save("delta_alpha_23_190814",delta_alpha_23) 
plt.xlabel( r"$\alpha$", fontsize=16 )
plt.ylabel( r"$Y(\alpha)$", fontsize=16 )
plt.grid(True)
plt.show()



alph_arr=np.linspace(0.2,2.9,1000)
#print alph_arr

delta_alpha_23_190412=np.load("delta_alpha_23_190412.npy")
kernel = stats.gaussian_kde(delta_alpha_23_190412)
pdf_alpha_190412=kernel(alph_arr)

delta_alpha_23_190814=np.load("delta_alpha_23_190814.npy")
kernel = stats.gaussian_kde(delta_alpha_23_190814)
pdf_alpha_190814=kernel(alph_arr)

plt.plot(alph_arr,pdf_alpha_190814,'r',label="GW190814")
plt.plot(alph_arr,pdf_alpha_190412,'b',label="GW190412")

#n, bins, patches = plt.hist(delta_alpha_23, 10, density=True, facecolor='g', alpha=0.75)
plt.plot(alph_arr,pdf_alpha_190814)
plt.plot(alph_arr,pdf_alpha_190412)

plt.xlabel(r"$\Delta\alpha_{23}$", fontsize=16)
plt.ylabel(r"$P(\Delta\alpha_{23})$", fontsize=16)
#plt.title(r"190412, KDE PDF $\Delta \alpha_{23}$")
#plt.text(60, .025, r'$\mu=100,\ \sigma=15$')
plt.xlim(0.35, 0.75)
#plt.ylim(0, 0.03)
#plt.grid(True)
plt.legend()
plt.savefig("190412_190814_KDE_alpha23.png",dpi=300)
plt.show()





