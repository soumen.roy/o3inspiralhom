#!/usr/bin/env python
## rahul.kashyap/14.Mar.2020: 
## 
# -*- coding: utf-8 -*-
__author__ = "Rahul Kashyap, Soumen Roy"
__email__ = "rkk5314@psu.edu"
__status__ = "Development"

""" This code plots the ratio of peak amplitudes of 33 and 22 modes for a random
    sample of binary black holes varying their masses and inclination angle of total angular momentum
    w.r.t. line of sight. 
"""

from __future__ import division
import sys
sys.path.append("o3inspiralhom/scripts/")

import lal
import lalsimulation as lalsim
import pycbc.frame
from pycbc.psd import welch, interpolate
from pycbc.filter import highpass_fir, resample_to_delta_t, sigmasq

import os, sys
import numpy as np

import matplotlib
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
matplotlib.rcParams['text.latex.preamble']=[r"\usepackage{amsmath}"]
matplotlib.rcParams['xtick.labelsize'] = 15
matplotlib.rcParams['ytick.labelsize'] = 15

from calndm import FindNonDimensionalFrequency
import utils
#mUtils = utils.MiscellaneousUtils()


def h(m1,m2,iota,mode_list):
    mUtils = utils.MiscellaneousUtils()
    mUtils.approx =  'SEOBNRv4HM' #'IMRPhenomHM' #  'SEOBNRv4HM'
    mUtils.deltaT = 1.0/4096.0
    mUtils.fmax = 0.5/mUtils.deltaT
    mUtils.deltaF = 1.0/5.0

    mUtils.fmin = 50.0

    mUtils.tref = 4.0
    mUtils.det = "L1"
    
    mUtils.mass1 = m1
    mUtils.mass2 = m2
    mUtils.iota = iota
    mUtils.mode_list=mode_list #[(l,m),(2,2)]

    wf0 = mUtils._gen_waveform()
    return wf0


n=2000
m1m2i_min=[1,1,0.1]
m1m2i_max=[100,100,3.14]
data = np.random.uniform(low=m1m2i_min, high=m1m2i_max, size=(n,3))

import time
t0 = time.time()
mode_ratio=[]

print 'for m1,m2,iota; the ratio of peak ampllitude in 33 mode divided by 22 mode'

for d in data:
    m1=d[0];m2=d[1];iota=d[2]
    if(m1>m2):
    	print  d[0],d[1],d[2],abs(h(m1,m2,iota,[(3,3)]).data.data).max()/abs(h(m1,m2,iota,[(2,2)]).data.data).max()
    	mode_ratio.append([d,abs(h(m1,m2,iota,[(3,3)]).data.data).max()/abs(h(m1,m2,iota,[(2,2)]).data.data).max()])
    
    

np.save('mode_ratio_dist_%s.npy'%str(n),mode_ratio)

print(time.time()-t0)

