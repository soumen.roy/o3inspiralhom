#!/usr/bin/env python
## rahul.kashyap/15.Mar.2020: 
## 
# -*- coding: utf-8 -*-
__author__ = "Rahul Kashyap, Soumen Roy"
__email__ = "rkk5314@psu.edu"
__status__ = "Development"

""" This code analyzes the waveform and generate Salpha plot for a series of injections 
    given in the folder frame_path below. 
"""


from __future__ import division
import glob
import re
import os,sys

import scipy.fftpack 
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
matplotlib.rcParams['text.latex.preamble']=[r"\usepackage{amsmath}"]
matplotlib.rcParams['xtick.labelsize'] = 15
matplotlib.rcParams['ytick.labelsize'] = 15


import lal
import lalsimulation as lalsim
import pycbc.frame
from pycbc.psd import welch, interpolate
from pycbc.filter import highpass_fir, resample_to_delta_t, sigmasq

sys.path.append("o3inspiralhom/scripts/")
from calndm import FindNonDimensionalFrequency
import utils
mUtils = utils.MiscellaneousUtils()


### code ###

frame_path="/home/rahul.kashyap/o3_highermodes/injection_o3HMinsp/HOM_noiseless_inj/"

info_list=glob.glob(frame_path+'inj*/INFO')
#print info_list
H1_list=glob.glob(frame_path+'inj*/*H1.txt')
#print H1_list


for filename,H1_data in zip(info_list,H1_list):#[0:1]:
    inj = filename.split('/')[6]
    f=open(filename)
    lines=f.readlines()
    #print lines[1]
    m1 = float(lines[1].split('=')[1].split('\n')[0])
    m2 = float(lines[2].split('=')[1].split('\n')[0])
    print m1,m2
    #print(re.findall('\d*?\.\d+', lines[1]))
    wf1=np.loadtxt(H1_data)
    #plt.plot(data)
    #plt.show()


    indx = np.where(wf1==np.max(wf1))[0][0]
    print(indx)
    wf1=wf1[indx-int(3.8*4096):indx+1*4096]  # 3.8 s before and 1 s after merger i.e. peak
    print(len(wf1)/4096)
    plt.plot(wf1)
    plt.show()

    ## Set the parameters of the waveform
    #mUtils.approx = 'SEOBNRv4HM'
    mUtils.deltaT = 1.0/4096.0
    mUtils.fmax = 0.5/mUtils.deltaT
    mUtils.deltaF = 1.0/5.0   #should not be too small i.e. smaller data segment 

    mUtils.fmin = 15.0  #LIGO starting band

    mUtils.tref = 4.0  # this should be the merger time; peak of say 2,2 mode 
    #mUtils.det = "L1"
    mUtils.mass1 = m1
    mUtils.mass2 = m2
    #mUtils.iota = 0.9

    
    wf = pycbc.types.TimeSeries( wf1, delta_t=mUtils.deltaT )

    # Multiply by Tukey window to taper at the start point.
    wf.data *= mUtils._gen_tukey_window(len(wf), beta=0.05, start=True, end=False)

    length = int(1.0/mUtils.deltaT/mUtils.deltaF)
    wf.resize( length )


    # Set the peak index at mUtils.tref
    peak_indx = wf.abs_arg_max()
    col_indx = int( mUtils.tref/mUtils.deltaT)
    shift_indx = col_indx-peak_indx
    wf.roll(shift_indx)
    
    
    plt.plot(wf.sample_times, wf.data)
    plt.clf()
    psd=pycbc.types.FrequencySeries(np.ones(len(wf.data)//2+1),mUtils.deltaF)  #no noise 
    whiten_wf = mUtils._whiten(wf, psd)
    plt.plot( whiten_wf.sample_times, whiten_wf.data)
    plt.show()
    
    ### Generate the time Frequency map
    mUtils.cwt_fhigh= 600.0
    mUtils.alpha_arr = np.linspace(0.2, 2.9, 541)
    mUtils.ndm_freq = 15.0
    mUtils.wavelet_Cg = 30.0
    cmplx_cwt, scales, freqs, coi, fftfreqs = mUtils._compute_cwt(whiten_wf,norm=True)

    # Take absolute square
    wf_power = np.abs(cmplx_cwt)**2.0


    ### Generate the time-frequency track of the (2, 2) mode
    # set a waverom, not necessary to be a HM waveform
    mUtils.approx = 'IMRPhenomD'
    fSchwarzISCO = 1.0 / ( 6.0**1.5 * lal.PI * (mUtils.mass1 + mUtils.mass2) * lal.MTSUN_SI )
    mUtils.track_end_frequency = 0.6*fSchwarzISCO
    mUtils.track_max_length = 0.5


    tArrFixDT, fArrFixDT, interpF, interpT = \
               mUtils._gen_time_frequency_path(start_freq=mUtils.fmin)
    
    
    fig, ax = plt.subplots(figsize=(8, 6))
    im = ax.imshow( wf_power , extent=[0.0, 1.0/mUtils.deltaF, freqs.min(), freqs.max()],\
                aspect='auto', origin='lower')
    ax.plot(tArrFixDT, fArrFixDT, ls='--', c='r', lw=0.9,label='m1=%s m2=%s'%(m1,m2))
    #ax.plot(tArrFixDT, 1.5*fArrFixDT, ls='--', c='white', lw=0.5)
    ax.set_ylim(0.0, 250)
    ax.set_xlim(3.0, 4.2)
    plt.xlabel('time[s]',fontsize=16)
    fig.colorbar(im)
    plt.legend(fontsize=16)
    plt.savefig(frame_path+'/S_alpha_noiseless_figures/%s_t-freq.pdf'%inj,dpi=100)
    #plt.show()

    plt.clf()
    ## Generate S(\alpha) from TF map
    Salpha = mUtils._stack_pixels_energies(wf_power, tArrFixDT, fArrFixDT, interpT)
    plt.semilogy(mUtils.alpha_arr, Salpha,label='m1=%s m2=%s'%(m1,m2))
    #plt.xlim([1,2])
    plt.ylim([1.e-52,4.e-48])
    plt.xlabel(r'$\alpha$',fontsize=16)
    plt.ylabel(r'$Y(\alpha)$',fontsize=16)
    plt.grid(True)
    plt.legend(fontsize=16)
    plt.tight_layout()
    plt.savefig(frame_path+'/S_alpha_noiseless_figures/%s_Salpha.pdf'%inj,dpi=100)
    #plt.show()
