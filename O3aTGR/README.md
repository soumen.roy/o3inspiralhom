# O3a TGR project

This project contains the contribution on Testing GR using inspiral higher-order modes from O3a LIGO events. The wiki page is [**here**](https://git.ligo.org/soumen.roy/o3inspiralhom/-/wikis/O3a-Testing-GR).