# Analysis on the inspiral higher-order mode detection from GW190814

GW190814 was observed on 14-08-2019 at 05:30:44 UTC. The FAR for this event was 2.033e-33, with a matched filter network SNR of  24.4970 . Click [here](https://gracedb.ligo.org/events/G347305/view/) for more details.


### Read and plot results using the python script

`python read_results.py --input-file gw190814_L1_C01_4096_0p6fISCO_0p45_strip.h5 --event-name gw190814`

* The results are produced using the C01 data, [details](https://git.ligo.org/soumen.roy/o3inspiralhom/-/blob/master/GW190814/analysis/test_insphom_GW190814_C01_Fs4096.sh).