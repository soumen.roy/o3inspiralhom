python read_insphom_results.py --input-file gw190814_L1_C01_4096_0p6fISCO_0p45_strip.h5 \
                        --event-name gw190814 \
                        --tfmap-tmin -3.0 \
                        --tfmap-fmax 200.0 \
                        --yalpha-subax-ymin 0.15 \
                        --yalpha-subax-ymax 2.5 \
                        --yalpha-ymax 9.5
