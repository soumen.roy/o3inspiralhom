## Link of the parameter estimation page
## https://ldas-jobs.ligo.caltech.edu/~charlie.hoy/PE/O3/S190814bv/C01/SEOBNRv4_ROM/samples/SEOBNRv4_ROM_pesummary.dat

## https://ldas-jobs.ligo.caltech.edu/~charlie.hoy/PE/O3/S190814bv/C01/SEOBNRv4_ROM/config/SEOBNRv4_ROM_config.ini

## Link of the science segment file and veto segment file
## https://ldas-jobs.ligo.caltech.edu/~shreejit.jadhav/o3/runs/hl/c01/a16_initial/1._analysis_time/1.01_segment_data/

python inspiralhom_analysis.py --detector L1 \
            --frame-type L1_HOFT_C01 --channel-name L1:DCS-CALIB_STRAIN_CLEAN_C01 \
            --science-segment-file H1L1-SCIENCE-1249243020-695617.xml \
            --veto-segment-file H1L1-VETOES-1249243020-695617.xml \
            --window-width 64000 --number-threads 35 \
            --approximant SEOBNRv4HM \
            --track-end-frequency 0.6fISCO \
            --track-max-length 0.45 \
            --mass1 22.01977017636057  --mass2 2.906081748755206   \
            --spin1z -0.08157500926149996   --spin2z -0.5527017237495524  \
            --event-time 1249852256.9926114 \
            --det-event-time 1249852257.0137289  \
            --deltaT 0.000244140625 \
            --output-filename gw190814_L1_C01_4096_0p6fISCO_0p45.h5
