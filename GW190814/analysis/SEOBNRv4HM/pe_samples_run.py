import os
import numpy as np

# web link: https://ldas-jobs.ligo.caltech.edu/~charlie.hoy/PE/O3/S190814bv/C01/SEOBNRv4HM/samples/SEOBNRv4HM_pesummary.dat


f = open('../SEOBNRv4HM_pesummary.dat', 'r')
data = []
for line in f.readlines():
    data.append(line.split() )
    
param_names = [x for x in data[0]]
samples = []
for ii in range(1, len(data)):
    samples.append([ float(data[ii][jj]) for jj in range( len(data[ii]) ) ]  )
samples = np.array(samples)

indx = np.where( samples[ :,param_names.index('log_likelihood') ] == \
                np.max( samples[ :,param_names.index('log_likelihood') ] ) )[0][0]


mass1 = samples[ :,param_names.index('mass_1') ][indx] 
mass2 = samples[ :,param_names.index('mass_2') ][indx] 
spin1z = samples[ :,param_names.index('spin_1z') ][indx] 
spin2z = samples[ :,param_names.index('spin_2z') ][indx] 
geotime = samples[ :,param_names.index('geocent_time') ][indx]

dtime = samples[ :,param_names.index('L1_time') ][indx] 


term1 = "python inspiralhom_analysis.py --detector L1 --frame-type L1_HOFT_C01" + \
         " --channel-name L1:DCS-CALIB_STRAIN_CLEAN_C01 " + \
            " --science-segment-file ../H1L1-SCIENCE-1249243020-695617.xml " +  \
            " --veto-segment-file ../H1L1-VETOES-1249243020-695617.xml " + \
            " --window-width 40000 --number-threads 40 " + \
            " --approximant SEOBNRv4HM " + \
            " --track-end-frequency 0.6fISCO " + \
            " --track-max-length 0.45 " + \
            " --ndm-freq 22.24686024806257 " 
        


sorted_indices = np.argsort( samples[ :,param_names.index('log_likelihood') ] )[::-1]

for ii in range(101):
    
    indx = sorted_indices[ii]
    
    mass1 = samples[ :,param_names.index('mass_1') ][indx] 
    mass2 = samples[ :,param_names.index('mass_2') ][indx] 
    spin1z = samples[ :,param_names.index('spin_1z') ][indx] 
    spin2z = samples[ :,param_names.index('spin_2z') ][indx] 
    geotime = samples[ :,param_names.index('geocent_time') ][indx]
    dtime = samples[ :,param_names.index('L1_time') ][indx]
    
    term2 = " --event-time %s "%geotime + " --det-event-time %s " %dtime + \
            " --mass1 %s "% mass1 + " --mass2 %s "% mass2 + \
            " --spin1z %s "%spin1z + " --spin2z %s "%spin2z
                
    term3 = " --deltaT 0.000244140625 --output-filename data/gw190814_L1_C01_60Hz_4096_0p6fISCO_0p45_%s.h5 "%str(ii)
    
    script = term1 + term2 + term3 
    #print script
    os.system( script )
                
