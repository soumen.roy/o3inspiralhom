# Check veto and science segment

The veto amnd science segment are not clearly visble from the plot due to longer time series.
Moving animation over time is suitable to see the segments, the animation is available in CIT cluster: `/home/soumen.roy/HHM/o3inspiralhom/inspiral_hom_contribution/ScienceSegments/FinalCode/GW190814/segment_video.avi`
or follow the [link](https://ldas-jobs.ligo.caltech.edu/~soumen.roy/o3inspiralhom/GW190814/segment_video.avi).